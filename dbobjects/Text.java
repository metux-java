
package org.de.metux.dbObjects;

import org.de.metux.util.*;

/* this interface represents datatypes for SQL-based databases */

public class Text extends Data
{
    String value;
    public Text ( String s )
    {
	value = s;
    }
    
    public String toSQL ()
    {
	return toSQL(value);
    }
    
    public static String toSQL(String str)
    {
	return "'" + 
	    StrReplace.replace("'", "\\'", 
	    StrReplace.replace("\\", "\\\\", str)) + "'";	    
    }
}
