
VERSION=0.1.1

## install directories
PREFIX?=$(HOME)/.usr
LIBDIR?=$(PREFIX)/lib
DATADIR?=$(PREFIX)/share
PKGCONFIGDIR?=$(LIBDIR)/pkgconfig

## build commands
ANT?=ant


JAR_INSTALL_DIR=$(DATADIR)/metux-java
JAR_INSTALL_FILE=$(JAR_INSTALL_DIR)/metux-java.jar
JAR_FILE=.build/metux-java.jar
PKGCONFIG_FILE=jar.metux-java.pc


all:	$(JAR_FILE) $(PKGCONFIG_FILE)

clean:
	rm -f $(JAR_FILE) $(PKGCONFIG_FILE)

build:		$(JAR_FILE) $(PKGCONFIG_FILE)

install:	$(JAR_FILE) $(PKGCONFIG_FILE)
	mkdir -p $(DESTDIR)$(JAR_INSTALL_DIR) $(DESTDIR)$(PKGCONFIGDIR)
	cp $(JAR_FILE) $(DESTDIR)$(JAR_INSTALL_DIR)
	cp $(PKGCONFIG_FILE) $(DESTDIR)$(PKGCONFIGDIR)

$(JAR_FILE):
	$(ANT) compile

$(PKGCONFIG_FILE):	$(PKGCONFIG_FILE).in
	cat $< \
	    | sed -e 's~@prefix@~$(PREFIX)~g'			\
	    | sed -e 's~@version@~$(VERSION)~g'			\
	    | sed -e 's~@datadir@~$(DATADIR)~g'			\
	    | sed -e 's~@jarfile@~$(JAR_INSTALL_FILE)~g'	\
	    | sed -e 's~@classpath@~$(JAR_INSTALL_FILE)~g'	\
	    >$@
