
package org.de.metux.util;

public class Depend
{
    public Version version;
    public String version_str   = "0.0.0.0";
    public String package_name;
    public String depend_str;
    public String[] features;
    
    public Depend (String text)
    {
	String[] myfeatures = new String[30];
	int num_features = 0;
	
	depend_str = text;

	int x,y;

	/* this is the position where the name part ends in our string */
	int name_end = -1;
	int parse_ptr = 0;
	
	/* cut out eventual version information */
	if ((x=text.indexOf("(>="))!=-1)
	{
	    if (x==0)
		throw new RuntimeException("parse error: missing name part");

	    y=text.indexOf(')', x+1);
	    if (y==-1)
		throw new RuntimeException("parse error: missing closing bracket");

	    version_str = text.substring(x+3,y);
	    name_end = x;
	    parse_ptr = y;
	}
	
	/* cut out features */
	while ((x=text.indexOf("[",parse_ptr))!=-1)
	{
	    if ((y=text.indexOf("]",x+1))==-1)
		throw new RuntimeException("parse error: missing ]");
	    myfeatures[num_features++] = text.substring(x+1,y);
	    parse_ptr = y;
	    if (name_end==-1)
		name_end = x;
	}
	if (num_features>0)
	{
	    features = new String[num_features];
	    for (int a=0; a<num_features; a++)
	    {
		features[a] = myfeatures[a];
	    }
	}
	
	if (name_end==-1)
	    package_name = text;
	else
	    package_name = text.substring(0,name_end);

	version = new Version(version_str);
    }
}
