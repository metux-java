
package org.de.metux.log;

public interface ILogger
{
//    public static final int LL_DEBUG   = 5;
//    public static final int LL_NOTICE  = 4;
//    public static final int LL_WARNING = 3;
//    public static final int LL_ERROR   = 2;
    
    public abstract void error  (String stage, String message);
    public abstract void warning(String stage, String message);
    public abstract void debug  (String stage, String message);
    public abstract void notice (String stage, String message);

    public abstract void setLogLevel(int ll);
}
