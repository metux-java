
package org.de.metux.ip;

import org.de.metux.util.Exec;
import java.net.UnknownHostException;

public class IP_Route 
{
    /* add an route for some prefix (ie. 10.6.4.0/24) via some gateway */
    /* FIXME: do some escaping to prevent shell string attacks */
    public static void addRoutePrefixGW(String target, String gw)
	throws UnknownHostException
    {
	Exec.exec_command(
	    "ip route add \""+
	    target+
	    "\" via \""+
	    Lookup.getIP_str(gw)+
	    "\""
	);
    }
}
