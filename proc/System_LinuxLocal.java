
package org.de.metux.proc;

import org.de.metux.util.Exec;
import java.io.File;
import java.lang.NumberFormatException;

/* hmm. seems this code only works w/ kaffe/classpath :( */

public class System_LinuxLocal implements ISystem
{
    public static final String proc_prefix = "/proc/";

    private class ProcInf implements IProcess
    {
	int pid;
	ProcInf(int my_pid)
	{
	    pid = 0;
	    this.pid = my_pid;
	}
	
	public boolean signal(String sig)
	{
	    if (sig==null) 
		throw new NullPointerException("parameter SIGNAL is NULL");

	    for (int x=0; x<valid_signals.length; x++)
	    {
		if (sig.equals(valid_signals[x]))
		{
		    System.err.println("GOT valid signame: "+sig);
		    System.err.println("OUT="+
			new Exec().run_catch("kill -"+sig+" "+pid));
		}
	    
	    }
	    return false;

	}	    

	public int getPID()
	{
	    return pid;
	}
    }

    public String getHostname()
    {
	return new Exec().run_catch("hostname").trim();
    }

    public int[] getPIDs()
    {
	int sz = 0;
	File f = new File (proc_prefix);
	String entries[] = f.list();
	
	// pids in ints umwandeln. fixme: evtl. noch auf dir checken
	int [] pids = new int[entries.length];
	for (int x=0; x<entries.length; x++)
	{
	    try
	    {
		int a = Integer.parseInt(entries[x],10);
		pids[sz++] = a;
	    }
	    catch (NumberFormatException e)
	    {
	    }
	}	
	
	// compress the array, so that the length is the actual
	// number of processes. leave no gaps. 
	int [] pids2 = new int[sz];
	for (int x=0; x<sz; x++)
	    pids2[x] = pids[x];
	
	return pids2;
    }
    
    public IProcess getProcess(int pid)
    {

// not finished yet :(
//	File handle = new File(proc_prefix+pid+"/");
//	Process_LinuxLocal process = new Process_LinuxLocal();
	
	
	return null;
    }
}
