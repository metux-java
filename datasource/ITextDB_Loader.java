package org.de.metux.datasource;

import java.io.File;
import java.util.Properties;

public interface ITextDB_Loader
{
    public void setCaching(boolean enable);
    public void clearCache();
    public Properties load(String filename);
    public Properties load(File filename);
}
