
all:
	for i in $(SUBDIRS); do make -C $$i || exit 1; done

clean:
	for i in $(SUBDIRS); do make -C $$i clean ; done

install:
	if [ ! "$(INSTALL_PATH_CLASSES)" ]; then echo "class install path not defined"; exit 1 ; fi
	mkdir -p $(INSTALL_PATH_CLASSES)
	for i in .build/* ; do if [ ! "$$i" == ".build/_unit_test.class" ]; then cp $(INSTALL_PATH_CLASSES) ; fi
#	cp -R .build/* $(INSTALL_PATH_CLASSES)
	for i in $(INSTALL_ADDITIONAL_CLASSES) ; do echo $$i ; cp -R $$i $(INSTALL_PATH_CLASSES) ; done
