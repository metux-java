
package org.de.metux.util;

import java.io.File;

public class mkdir
{
    public static void mkdir_recursive(String name)
    {
	String absdir = new File(name).getAbsolutePath();
	String[] pathnames = StrSplit.split(absdir,"/");
	String mypath = "";
	for (int x=0; x<pathnames.length; x++)
	{
	    mypath += "/"+pathnames[x];
//	    System.err.println("Creating: "+mypath);
	    new File(mypath).mkdir();
	}
    }
}
