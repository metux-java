
package org.de.metux.util;

import java.io.IOException;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class LoadFile
{
    public static String load(URL url)
	throws IOException
    {
	return load(url,false);
    }

    public static String load(URL url, boolean strip_comments)
	throws IOException
    {
	URLConnection conn = url.openConnection();
	conn.setDoOutput(true);

	/* read back */	
	BufferedReader rd = 
	    new BufferedReader(new InputStreamReader(conn.getInputStream()));
	String text="";
	String line;
	while ((line=rd.readLine()) != null)
	{
	    int x;
	    if (strip_comments && ((x=line.indexOf('#'))>-1))
		line = line.substring(0,x);
	    text += line.trim()+"\n";
	}
	return text;
    }

    public static String load(File filename, boolean strip_comments)
	throws IOException
    {
	BufferedReader in;
	
	try
	{
	    in = new BufferedReader(new FileReader(filename));
	}
	catch (FileNotFoundException e)
	{
	    return null;
	}
    
	String text = "";
	String line;
	while ((line=in.readLine())!=null)
	{
	    int x;
	    // strip off comments
		    
	    if (strip_comments && ((x=line.indexOf('#'))>-1))
		line = line.substring(0,x);
		
	    text += line.trim() + "\n";
	}
	
	return text.trim();
    }

	public static String load(Filename filename, boolean strip_comments)
		throws IOException
	{
		BufferedReader in = new BufferedReader(filename.getReader());
		String text = "";
		String line;
		while ((line=in.readLine())!=null)
		{
			int x;
			// strip off comments
			if (strip_comments && ((x=line.indexOf('#'))>-1))
				line = line.substring(0,x);
			text += line.trim() + "\n";
		}
		return text.trim();
	}

	public static String load_def(Filename filename, boolean strip_comments, String def)
		throws IOException
	{
		try
		{
			return load(filename, strip_comments);
		}
		catch (FileNotFoundException e)
		{
			return def;
		}
	}
}
