
package org.de.metux.util;

public class SQL_Encode
{
    public static String Text(String s)
    {
	if (s==null)
	    return null;
	
	return "'"+escape(s)+"'";
    }

    public static String escape(String s)
    {
	if (s==null)
	    return "";

	char[] chars = s.toCharArray();
	
	String res = "";
	for (int x=0; x<chars.length; x++)
	{
	    switch (chars[x])
	    {
		case '\'':	res += "\\'";		break;
		case '"':	res += "\\\"";		break;
		default:	res += chars[x];	break;
	    }
	}
	return res;
    }
    
    public static String Name(String s)
    {
	if (s==null)
	    return "";
	return "\""+escape(s)+"\"";
    }

    public static String Float(float f)
    {
	return "'"+f+"'";
    }
    
    public static String Oid(long oid)
    {
	return String.valueOf(oid);
    }
}
