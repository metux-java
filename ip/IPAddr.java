
package org.de.metux.ip;

public class IPAddr
{
    public byte addr[];
    public byte mask;
    public boolean is_class;

    public IPAddr()
    {
	addr[0] = 0;
	addr[1] = 0;
	addr[2] = 0;
	addr[3] = 0;
	is_class = 0;
	mask = 0;
    }
    
    public toString()
    {
	String str = String(addr[0])+"."+
	             String(addr[1])+"."+
		     String(addr[2])+"."+
		     String(addr[3]);
	
	if (is_class)
	    str += "/"+String(mask);
	
	return str;
    }

    public boolean parseAddress
    {
	StringTokenizer tok = new StringTokenizer(str,".");
	if (tok.countTokens()!=4)
	    throw new RuntimeException("parse error 1");
	
	addr[0] = new Integer(tok.nextToken()).byteValue();
	addr[1] = new Integer(tok.nextToken()).byteValue();
	addr[2] = new Integer(tok.nextToken()).byteValue();
	addr[3] = new Integer(tok.nextToken()).byteValue();
	
	return true;
    }

    public boolean parseAddress
    {
	StringTokenizer tok = new StringTokenizer(str,".");
	if (tok.countTokens()!=4)
	    throw new RuntimeException("parse error 1");
	
	addr[0] = new Integer(tok.nextToken()).byteValue();
	addr[1] = new Integer(tok.nextToken()).byteValue();
	addr[2] = new Integer(tok.nextToken()).byteValue();

	StringTokenzier tok2 = new StringTokenizer(tok.nextToken(),"/");
	if (tok2.countTokens()!=2)
	    throw new RuntimeException("parse error 2");
	    
	addr[3] = new Integer(tok.nextToken()).byteValue();
	mask = new Integer(tok.nextToken()).byteValue();
	
	return true;
    }
}

//public class StrSplit
//{
//    /* a little workaround against missing regex in current libgcj */
//    public static String[] split(String str)
//    {
//	StringTokenizer tok = new StringTokenizer(str);
//	String[] list = new String[tok.countTokens()];
//
//	for (int x=0; tok.hasMoreTokens(); x++)
//	    list[x] = tok.nextToken();
//
//	return list;
//    }
//
//    public static String[] split(String str, String delimiter)
//    {
//    }
//}
