
package org.de.metux.propertylist;

public interface IPostprocessor
{
    /* run postprocessing after some db source was loaded 
       (ie. if some specials have to be interpreted for loading includes */

    // returns TRUE if rerun is necessary (ie. loaded more dbsources)
    public boolean propertylist_postprocess(IPropertylist propertylist)
	throws EIllegalValue;
}
