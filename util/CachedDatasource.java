
package org.de.metux.util;

import java.net.URL;
import java.io.IOException;
import java.util.Hashtable;
import java.io.File;
import java.io.FileNotFoundException;

public class CachedDatasource
{
    private Hashtable cache = new Hashtable();
    public boolean enabled = true;

    public String loadContent(URL url, boolean strip_comments)
    {
	String filename = url.toString();

	if (!enabled)
	{
	    try{
		return LoadFile.load(url,strip_comments);
	    }
	    catch (IOException e)
	    {
		return null;
	    }
	}
	
	String key = "::URL::CONTENT::"+(strip_comments?"STRIP::":"")+url;
	String content;
	
	try
	{
	    content = (String)cache.get(key);
	}
	catch (ClassCastException e)
	{
	    /* we've got an cached NOT-FOUND */
	    return null;
	}

	if (content != null)
	    return content;

	try
	{
	    content = LoadFile.load(url,strip_comments);
	    cache.put(key,content);
	    return content;
	}
	catch (IOException e)
	{
	    /* put the error into the cache */
	    cache.put(key,e);
	    return null;
	}
    }
    
    public String loadContent(File filename, boolean strip_comments)
    {
	if (!enabled)
	{
	    try{
		return LoadFile.load(filename,strip_comments);
	    }
	    catch (IOException e)
	    {
		return null;
	    }
	}
	
	String key = "::FILENAME::CONTENT::"+(strip_comments?"STRIP::":"")+filename;
	String content;
	
	try
	{
	    content = (String)cache.get(key);
	}
	catch (ClassCastException e)
	{
	    /* we've got an cached NOT-FOUND */
	    return null;
	}

	if (content != null)
	    return content;

	try
	{
	    content = LoadFile.load(filename,strip_comments);
	    if (content==null)
		throw new RuntimeException("Could not load file: "+filename);
	    cache.put(key,content);
	    return content;
	}
	catch (IOException e)
	{
	    /* put the error into the cache */
	    cache.put(key,e);
	    return null;
	}
    }

    public Hashtable loadTextTable(String fn, String pkey)
    {
	Hashtable ht;
	String key = "::FILENAME::TEXTDB::["+pkey+"]"+fn;
	
	if (!enabled)
	    return TextTable.Load(fn,pkey);
	
	try
	{
	    ht = (Hashtable)cache.get(key);
	    if (ht!=null)
		return ht;
	}
	catch (ClassCastException e)
	{
	    return null;
	}

	ht = TextTable.Load(fn,pkey);
	if (ht==null)
	{
	    cache.put(key,new FileNotFoundException(fn));
	    return null;
	}
	    
	cache.put(key,ht);
	return ht;
    }
}
