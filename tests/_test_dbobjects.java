
import org.de.metux.util.*;
import org.de.metux.dbObjects.*;

public class _unit_test
{
    public static void main(String[] args) {}
}
/*
public class _unit_test extends Tester
{
    public static void main(String[] args)
    {
	if (
	    test_str_replace() 	&&
	    test_sql_int()	&&
	    test_sql_text1()	&&
	    test_sql_text2()	&&
	    test_sql_text3()
	)
	    System.exit(0);
	    
	System.exit(0);    
    }
    
    / * test imported str_replace() function - just in case the compiler 
       makes trouble (gcj seems to have some bugs when linking against 
       several .JAR's * /
    public static boolean test_str_replace()
    {
	return runTestStr 
	(
	    "StrReplace",
	    "hello world 123 huh",
	    StrReplace.replace("<XXX>", "123", "hello world <XXX> huh" )
	);
    }

    public static boolean test_sql_int()
    {
	return runTestStr
	(
	    "sql_int", 
	    "1234", 
	    new Int ( 1234 ).toSQL()
	);
    }
    
    public static boolean test_sql_text1()
    {
	return runTestStr
	(
	    "sql_text1",
	    "'hello world'",
	    new Text ( "hello world" ).toSQL()
	);
    }

    public static boolean test_sql_text2()
    {
	return runTestStr
	(
	    "sql_text2",
	    "'\\'hello world\\''",
	    new Text ( "'hello world'" ).toSQL()
	);
    }

    public static boolean test_sql_text3()
    {
	return runTestStr
	(
	    "sql_text3\\",
	    "'\\'hello world\\\\'",
	    new Text ( "'hello world\\" ).toSQL()
	);
    }
}
*/
