
package org.de.metux.util;

import java.io.*;
import java.net.*;


public class Decompressor
{
    public boolean decompress(String archive, String target)
    {
	return decompress(archive,target,null);
    }
    
    public boolean decompress(String archive, String target, String format)
    {
	if ((format==null) || (format.length()==0))
	{
	    /* try to detect format */
	    	


    {
	String buildroot = config.cf_get_str_n("@@buildroot");
	String archiver = config.cf_get_str("archiver");
	if (archiver.length()==0)
	{
	    notice("no archiver given ... trying to detect" );
	    if (source.endsWith(".tar.gz") || 
	        source.endsWith(".tgz"))
		config.cf_set("archiver", archiver = "tar.gz");
	    else if (source.endsWith(".tar.bz2") ||
		     source.endsWith(".tbz") ||
		     source.endsWith(".tbz2"))
		config.cf_set("archiver", archiver = "tar.bz");
	    else
		return error("cannot detect proper archiver for \""+source+"\"");
	}	    
    
	if (!mkdir(buildroot))
	    return false;
	
	if (archiver.equals("tar.bz2"))
	{
	    notice("decompressing tar.bz2: "+source+" ..." );
	    return exec ( "cd "+buildroot+" && tar -xjf "+source );
	}
	
	if (archiver.equals("tar.gz"))
	{
	    notice("decompressing tar.gz: "+source+" ..." );
	    return exec ( "cd "+buildroot+" && tar -xzf "+source );
	}

	return error("unsupported archiver: "+archiver );
    }

    /* pre-execute something *before* we start decompressing
       ie. for preparing patch sets */
       
    boolean preexec()
    {
	/* prepare patches */
	String preexec=config.cf_get_str_n("prebuild-patch-preexec");
	if (preexec==null)
	    return true;
	    
	notice("Calling preexec: "+preexec);
	if (!exec(preexec))
	    return error("preexec failed");

	return true;
    }

    boolean cleanup()
    {
	String buildroot = config.cf_get_str_n("@@buildroot");
	notice("cleaning up "+buildroot );
	rm.remove_recursive(buildroot);
	mkdir(buildroot);
	return true;
    }
    
    boolean paranoia_check()
    {
	/* check for proper buildroot variable ... probably not really necessary */
	String buildroot = config.cf_get_str("@@buildroot");
	try
	{
	    buildroot = new File(buildroot).getCanonicalPath();
	}
	catch (IOException e)
	{
	    return error("exception: "+e);
	}
	
	if (buildroot.length()<5)
	    return error("corrupt @@buildroot variable - internal problem ?");
    
	/* -- get the source file name / uri -- */
	String source=config.cf_get_str_n("source-file");
	if (source==null)
	    return error("missing package source option in configuration");

	return true;
    }

    boolean decompress()
    {
	/* now run decompression */
	String srclist[] = config.cf_get_list("source-file");
	if (srclist.length==0)
	    return error("missing package source option in configuration");

	for (int x=0; x<srclist.length; x++)
	    if (!decompress_archive(srclist[x]))
		return false;

	return true;    
    }
        
    boolean prepare_sourcetree()
    {
	return 
	(	/* hope we've got shortcut evaluation! */
	    paranoia_check()	&&
	    preexec()		&&
	    cleanup()		&&
	    decompress()	&&
	    lookup_srcdir()	&&
	    apply_patches()
	);
    }

    /* right after decompression, we need to know our actual srcroot */
    boolean lookup_srcdir()
    {
	String buildroot = config.cf_get_str_n("@@buildroot");
	if (buildroot==null)
	    return error("lookup_srcdir: Internal error: @@buildroot not defined");

	File handle = new File(buildroot);
	String sublist[] = handle.list();
	
	if (sublist==null) 
	    return error("could not get directory listing!");
	    
	if (sublist.length==0)
	    return error("no srcdir found. seems like decompression failed");
	
	if (sublist.length>1)
	    return error("more than one srcdir found. looks like trouble");
	
	notice("srcdir is: "+sublist[0]);
	
	String srcroot = buildroot+"/"+sublist[0];
	notice("srcroot is: "+srcroot);
	
	config.cf_set("@@srcroot", srcroot);

	return true;
    }
    
    boolean apply_patches()
    {
	String patches[]  = config.cf_get_list("prebuild-patch-file");
	String patch_opts = config.cf_get_str("prebuild-patch-opts");
	String srcroot    = config.cf_get_str_n("@@srcroot");
	
	if (patches.length==0)
	    return true;
	    
	for (int x=0; x<patches.length; x++)
	{
	    if (!(new File(patches[x]).exists()))
		return error("could not load patch file: "+patches[x]);

	    notice("applying patch: "+patches[x]+"("+patch_opts+")" );
	    if (!exec("cd "+srcroot+";pwd;patch "+patch_opts+"<"+patches[x]))
		return false;
	}

	return true;
    }
}
