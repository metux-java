package org.de.metux.datasource;

import java.io.File;
import java.net.URL;

public interface IContent_Loader
{
    public void setCaching(boolean c);
    public void clearCache();

    public String load(URL url, boolean strip_comments);
    public String load(File f, boolean strip_comments);
    public String load(String fn, boolean strip_comments);
}
