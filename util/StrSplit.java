
package org.de.metux.util;

import java.util.*;

public class StrSplit
{
    /* a little workaround against missing regex in current libgcj */
    public static String[] split(String str)
    {
//	if (str==null)
//	    return split("");

	StringTokenizer tok = new StringTokenizer(str);
	String[] list = new String[tok.countTokens()];

	for (int x=0; tok.hasMoreTokens(); x++)
	    list[x] = tok.nextToken();

	return list;
    }

    public static String[] split(String str, String delimiter)
    {
//	if (str==null)
//	    return split("");

	StringTokenizer tok = new StringTokenizer(str,delimiter);
	String[] list = new String[tok.countTokens()];

	for (int x=0; tok.hasMoreTokens(); x++)
	    list[x] = tok.nextToken();

	return list;
    }
}
