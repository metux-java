package org.de.metux.datasource;

import java.util.Hashtable;

public class TextTable_List extends Hashtable<String,TextTable>
{
    public void setEntry(String name, TextTable entry)
    {
	put(name, entry);
    }

    public TextTable getEntry(String name)
    {
	return get(name);
    }
}
