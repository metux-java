
package org.de.metux.util;

import java.util.Hashtable;
import java.util.Enumeration;
import java.io.IOException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.BufferedReader;
import java.io.FileReader;

public class FileStock
{
    class record
    {
	String filename;
	String md5;
    }
    
    Hashtable ht;
    
    public FileStock()
    {
	ht = new Hashtable();
    }

    public String getmd5(String fn)
    {
	return getmd5(new File(fn));
    }

    public String getmd5(File f)
    {
	return FileOps.md5(f);
    }

    private void add(record r_new)
    {
	record r_old;
    
	if ((r_old = (record)ht.get(r_new.md5))==null)
	{
	    ht.put(r_new.md5,r_new);
	    return;
	}
	
    	if (r_new.md5.equals(r_old.md5))
	    return;
	    
	throw new RuntimeException("COLLISSION "+r_old.filename+" vs. "+r_new.filename);
    }

    public void add(String [] fns)
	throws FileNotFoundException, IOException
    {
	if (fns==null)
	    return;
	    
	for (int x=0; x<fns.length; x++)
	    add(fns[x]);
    }
    
    public void add(String fn)
	throws FileNotFoundException, IOException
    {
	record r = new record();
	r.filename = fn;
	r.md5      = getmd5(fn);
	add(r);
    }
    
    public String[] getFilenames()
    {
	int count = ht.size();
	String fns[] = new String[count];
	
	int x = 0;
	for (Enumeration e=ht.keys(); e.hasMoreElements(); )
	{
	    record r = (record)ht.get((String)e.nextElement());
	    fns[x++] = r.filename;
	}
	System.runFinalization();
	return fns;
    }
}
