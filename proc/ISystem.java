
package org.de.metux.proc;

public interface ISystem
{
    public String getHostname();
    public IProcess getProcess(int pid);
    public int[] getPIDs();

    public String valid_signals [] = 
	{ "HUP", "TERM", "KILL", "STOP", "CONT" };
}
