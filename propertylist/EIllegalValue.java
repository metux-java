
package org.de.metux.propertylist;

public class EIllegalValue extends Exception
{
    public String name;
    public String value;
	
    public EIllegalValue (String my_name, String my_value)
    {
	super(my_name+"=\""+my_value+"\"");
	name = my_name;
	value = my_value;
    }
    
    public EIllegalValue(String my_name, String my_value, Throwable my_cause)
    {
	super(my_name+"=\""+my_value+"\"", my_cause);
	name = my_name;
	value = my_value;
    }
}
