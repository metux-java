
package org.de.metux.util;

import java.util.Properties;
import java.util.Hashtable;
import java.io.FileNotFoundException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.net.URLDecoder;

/*
    Note: URLDecoder.decode(String text) is deprecated. 
    Instead .decode(String text, String enc) should be used.
    Have to think about it carefully.
*/

public class TextDB
{
    Properties hash = new Properties();
    
    public Hashtable getHashtable ()
    {
	return hash;
    }

    public Hashtable Load(String filename)
    {
	Hashtable h = new Hashtable ();
	if (LoadIntoHashtable(filename,h))
	    return h;
	else
	    return null;
    }

    public static boolean LoadIntoHashtable(String filename, Hashtable table)
    {
	BufferedReader in;

	try
	{
	    in = new BufferedReader(new FileReader(filename));
	}
	catch (FileNotFoundException e)
	{
	    return false;
	}

	try
	{
	    String line;
	    while ((line=in.readLine())!=null)
	    {
		int x;
		int len;
		
		/* strip off comments */
		if ((x=line.indexOf('#'))>=0)
		    line = line.substring(0,x);
		
		line = line.trim();
		if ((len=line.length())>0)
		{
		    /* -- split into key and value -- */
		    if ((x = line.indexOf(':'))>0)
	    	    {
			String key;
			String value;
			String str;
			
			key = (line.substring(0,x)).trim();
			value = (line.substring(x+1)).trim();
			value = URLDecoder.decode(value);
			
			if (table.containsKey(key))
			    table.put(key,table.get(key)+"\n"+value);
			else
			    table.put(key,value);
		    }
		    else
		        System.err.println ( "CORRUPT LINE "+line );
		}
	    }	    
	    return true;
	}
	catch(Exception e)
	{
	    System.err.println("File input error");
	    return false;
	}
    }

    public TextDB ( String filename ) throws FileNotFoundException
    {
	if (this.hash==null)
	    this.hash = new Properties();

	LoadIntoHashtable(filename,this.hash);
    }
}
