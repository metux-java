
package org.de.metux.propertylist;

public class EBrokenBoolRepresentation extends EIllegalValue
{
    public EBrokenBoolRepresentation(String name, String value)
    {
	super(name,value);
    }
}
