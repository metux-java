
package org.de.metux.util;

public final class StrReplace
{
    public final static String replace (
	String needle, 
	String to, 
	String haystack)
    {
	// haystack is the original string
	// needle   is the string to be replaced
	// to     is the string which will used to replace
	boolean restart = true;
	int needle_size = needle.length();
	while (restart)
	{
	    restart = false;	
	    int start = haystack.indexOf (needle);
	    if (start!=-1) 
	    {
		char [] haystackChars = haystack.toCharArray();
		StringBuffer buffer = new StringBuffer();
		int copyneedle=0;
		while (start != -1) 
		{
		    buffer.append (haystackChars, copyneedle, start-copyneedle);
		    buffer.append (to);
		    copyneedle=start+needle_size;
		    start = haystack.indexOf (needle, copyneedle);
		    restart = true;
		}
		buffer.append (haystackChars, copyneedle, haystackChars.length-copyneedle);
		haystack = buffer.toString();
	    }
	}

	// ugly hack !!!
	if (haystack.endsWith(needle))
	{
	    String r = haystack+" ";
	    String substring = haystack.substring(0,r.indexOf(needle))+to;
	    return substring;
	}

	return haystack;
    }
}
