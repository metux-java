/*
    This class represents a list of (unique) flags, which may exist,
    but only once at max. If we add some flag twice, nothing really
    happens. 
    
    Flaglists can also be used to collect things like search path
    arguments, etc.
    
    Operations:
    
	void add(String name) 	-> add some flag
	void clear(String name)	-> clear some flag
	String[] getFlagNames()	-> lists all flags to an String array

*/

package org.de.metux.util;

import java.util.Hashtable;
import java.util.Enumeration;

public class UniqueNameList
{
    Hashtable ht = new Hashtable();
    
    public void add(String name)
    {
	if ((name!=null)&&(name.length()!=0))
	    ht.put(name,name);    
    }
    
    public void add(String name[])
    {
	if (name!=null)
	    for (int x=0; x<name.length; x++)
		add(name[x]);
    }
    
    public void clear(String name)
    {
	if ((name!=null)&&(name.length()!=0))
	    ht.remove(name);
    }
    
    public String[] getNames()
    {
	String res[] = new String[ht.size()];
	int x=0;
	for (Enumeration e=ht.keys(); e.hasMoreElements();)
	    res[x++]=(String)e.nextElement();
	return res;
    }

    public boolean exists(String name)
    {
	return (ht.get(name) != null);
    }
    
    public String toString()
    {
	String s = null;
	for (Enumeration e=ht.keys(); e.hasMoreElements();)
	    s = ((s==null)?("{ \""):", \"")+e.nextElement()+"\"";
	return s+" }; ";
    }	
}
