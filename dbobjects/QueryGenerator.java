
package org.de.metux.dbObjects;

import java.util.*;

public class QueryGenerator
{
    static String InsertQuery(String tablename, DataList data)
    {
	String fields = null;
	String values = null;
	
	data.enumReset();
	while (data.enumScan())
	{
	    if (fields==null)
		fields += new String(data.walkName);
	    else
		fields += ", " + data.walkName;

	    if (values==null)
		values += data.walkValue.toSQL();
	    else
		values += ", " + data.walkValue.toSQL();
	}
	
	if ((fields==null)||(values==null))
	    return null;
	    
	return "INSERT INTO "+tablename+" ( "+fields+" ) VALUES ( "+values+" );";
    }

    static String UpdateQueryAND(String tablename, DataList search, DataList newdata)
    {
	/* generate WHERE clause */
	String q_search = null;
	search.enumReset();
	while (search.enumScan())
	{
	    if (q_search==null)
		q_search = search.walkName+"="+search.walkValue.toSQL();
	    else
		q_search += " AND "+search.walkName+"="+search.walkValue.toSQL();
	}
	
	/* generate SET ... clause */
	String q_newdata = null;
	newdata.enumReset();
	while (newdata.enumScan())
	{
	    if (q_newdata==null)
		q_newdata = newdata.walkName+"="+newdata.walkValue.toSQL();
	    else
		q_newdata += ", "+newdata.walkName+"="+newdata.walkValue.toSQL();
	}
	return "UPDATE "+tablename+" SET "+q_newdata+" WHERE "+q_search;
    }

    static String UpdateQueryAND(String tablename, String oidField, Data oidValue, DataList newdata)
    {
	/* generate SET ... clause */
	String q_newdata = null;
	newdata.enumReset();
	while (newdata.enumScan())
	{
	    if (q_newdata==null)
		q_newdata = newdata.walkName+"="+newdata.walkValue.toSQL();
	    else
		q_newdata += ", "+newdata.walkName+"="+newdata.walkValue.toSQL();
	}
	return "UPDATE "+tablename+" SET "+q_newdata+" WHERE "+oidField+"="+oidValue.toSQL();
    }
}
