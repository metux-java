
package org.de.metux.util;

public class Terminal
{
    public static String ESC_FG_GREEN 	= "\033[1;32m";
    public static String ESC_NORMAL	= "\033[m\017";
    public static String ESC_FG_YELLOW	= "\033[1;33m";
    public static String ESC_FG_BLUE	= "\033[1;34m";
    public static String ESC_FG_CYAN	= "\033[1;35m";
    public static String ESC_FG_RED	= "\033[1;31m";

    public static String render(String str)
    {
	return __render(__render(str));
    }
    public static String __render(String str)
    {
	return 
	    StrReplace.replace ( "{TERM_NORMAL}",       ESC_NORMAL,
	    StrReplace.replace ( "{TERM_FG_GREEN}", 	ESC_FG_GREEN,
	    StrReplace.replace ( "{TERM_FG_YELLOW}", 	ESC_FG_YELLOW,
	    StrReplace.replace ( "{TERM_FG_BLUE}",	ESC_FG_BLUE,
	    StrReplace.replace ( "{TERM_FG_CYAN}",	ESC_FG_CYAN,
	    StrReplace.replace ( "{TERM_FG_RED}",	ESC_FG_RED,
	    str ))))));
    }
}
