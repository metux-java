
package org.de.metux.ip;

import org.de.metux.util.Exec;
import java.net.UnknownHostException;

public class IPSEC_SA
{
    String spi;
    
    public IPSEC_SA(String spi_val)
    {
	spi = spi_val;
    }

    public boolean delete()
    {
	return Exec.exec_command("ipsecadm sa del --spi="+spi);
    }

    public boolean add(
	String source,
	String destination,
	String cipher,
	String cipher_keyfile,
	String digest,
	String digest_keyfile,
	boolean duplex
    )
	throws UnknownHostException
    {
	String cmdline = 
	    "ipsecadm sa add "+
	    " --spi="+spi+
	    " --src="+Lookup.getIP_str(source)+
	    " --dst="+Lookup.getIP_str(destination)+
	    " --cipher="+cipher+
	    " --cipher-keyfile="+cipher_keyfile+
	    " --digest="+digest+
	    " --digest-keyfile="+digest_keyfile+
	    (duplex ? " --duplex" : "");
	
	return Exec.exec_command(cmdline);
    }
}
