
package org.de.metux.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.xbill.DNS.Lookup;
import org.xbill.DNS.SimpleResolver;
import org.xbill.DNS.Record;
import org.xbill.DNS.ARecord;
import org.xbill.DNS.Type;

public class IPTools
{
    static public InetAddress getIP(String hostname, String server)
    {
	try
	{
	    Lookup l = new Lookup(hostname,Type.A);
	    if (server!=null)
		l.setResolver(new SimpleResolver(server));

	    l.run();
	    if (l.getResult()==Lookup.SUCCESSFUL)
	    {
	        Record res[] = l.getAnswers();
		for (int x=0; x<res.length; x++)
		{
		    ARecord arec = (ARecord)res[x];
		    return arec.getAddress();    
		}
	    }
	    else
		return null;
	}
	catch (Exception e)
	{
	    System.err.println("getIP() error: "+e);
	    return null;
	}
	
	return null;
    }

    static public InetAddress IPtoInetAddress(String ip)
    {
	try
	{
	    InetAddress addr = InetAddress.getByName(ip);
	    return addr;
	}
	catch(Exception ex)
	{
	    System.err.println("EX: "+ex);
	    return null;
	}	
    }

    static public InetAddress getInterfaceAddress(String ifname)
    {
	Exec e = new Exec();
	
	// FIMXE !!!
	String res = e.run_catch("/sbin/ifconfig "+ifname+" | grep \"inet addr\"");
	if (res==null)
	    return null;
	
	int pos_addr = res.indexOf("inet addr:");
	int pos_mask = res.indexOf(" ",pos_addr+11);

	String text_addr = res.substring(pos_addr+10,pos_mask).trim();

	return IPtoInetAddress(text_addr);
    }
}
