
package org.de.metux.log;

public class LoglevelID
{
    public static final int DEBUG   = 5;
    public static final int NOTICE  = 4;
    public static final int WARNING = 3;
    public static final int ERROR   = 2;
    public static final int NONE    = 0;
    
    public static int toID(String name)
    {
	return toID(name,NOTICE);
    }

    public static int toID(String name, int def)
    {
	if (name==null)
	    return def;
	
	name = name.toLowerCase();
	if (name.equals("debug"))
	    return DEBUG;
	if (name.equals("notice"))
	    return NOTICE;
	if (name.equals("warning"))
	    return WARNING;
	if (name.equals("error"))
	    return ERROR;
	
	return def;
    }
}
