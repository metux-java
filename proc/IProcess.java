
package org.de.metux.proc;

public interface IProcess
{
//    public String name  = "";
//    public String executable = "";
//    public int pid;
    
    // allowed signal names:
    // TERM, KILL, HUP, etc.
    public int getPID();
    public boolean signal(String sig);
}
