
package org.de.metux.log;

public class LoggerASCII implements ILogger
{
    public int loglevel = LoglevelID.DEBUG;
    
    final public void setLogLevel(int ll)
    {
	loglevel = ll;
    }

    final public void error(String stage, String message)
    {
	if (loglevel >= LoglevelID.ERROR)
	    System.err.println(" (ERROR) ["+stage+"] "+message);
    }
    
    final public void warning(String stage, String message)
    {
	if (loglevel >= LoglevelID.WARNING)
	    System.err.println("  (WARN) ["+stage+"] "+message);
    }
     
    final public void debug(String stage, String message)
    {
	if (loglevel >= LoglevelID.DEBUG)
    	    System.err.println(" (DEBUG) ["+stage+"] "+message);
    }
    
    final public void notice(String stage, String message)
    {
	if (loglevel >= LoglevelID.NOTICE)
	    System.err.println("\033[1;32m(NOTICE) ["+stage+"] "+message);
    }
}
