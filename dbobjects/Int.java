
package org.de.metux.dbObjects;

/* this interface represents datatypes for SQL-based databases */

public class Int extends Data
{
    int value;

    public Int()
    {
	value = -1;
    }
    
    public Int ( int i )
    {
	value = i;
    }
    
    public String toSQL ()
    {
	return new Integer(value).toString();
    }
    
    static String toSQL(int i)
    {
	return new Integer(i).toString();
    }
}
