
package org.de.metux.util;

import java.util.*;

// FIXME: should be obsoleted by class Properties

public class Stringtable
{
    private Hashtable h;

    public Stringtable(Hashtable ht)
    {
	h = ht;
    }
    
    public Stringtable()
    {
	h = new Hashtable();
    }

    public void remove(String key)
    {
	if (key==null) 
	    return;
	h.remove(key);
    }
	
    public String get(String key)
    {
	if ((key==null) || (key.equals("")))
	    return "";
	    
	return (String)h.get(key);
    }
    
    public void set(String key, String value)
    {
	if ((key==null)||(key.length()==0))
	    return;
	
	if ((value==null)||(value.length()==0))
	    h.remove(key);
	else
	    h.put(key,value);
    }
    
    public final void add(String key, String value)
    {
	if ((key==null)||(key.equals(""))||(value==null)||(value.equals("")))
	    return;
	    
	String ex = (String)h.get(key);
	if (ex==null)
	    h.put(key,value);
	else
	    h.put(key,ex+" "+value);
    }
    
    public final Enumeration keys()
    {
	return h.keys();
    }
}
