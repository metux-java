
package org.de.metux.dbObjects;

import java.util.*;

public class DataList
{
    Hashtable t;
    Enumeration e;
    
    String walkName;
    Data walkValue;
    
    DataList ()
    {
	t = new Hashtable();
    }
    
    public boolean Add (String name, Data d)
    {
	t.put(name,d);
	return true;
    }
    
    public Data Get (String name)
    {
	return (Data)t.get(name);
    }
    
    public void enumReset()
    {
	e = t.keys();
    }
    
    public boolean enumScan()
    {
	if (!e.hasMoreElements())
	    return false;

	walkName = (String)e.nextElement();
	walkValue = (Data)t.get(walkName);	    
	return true;
    }
}
