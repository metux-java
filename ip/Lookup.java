
package org.de.metux.ip;

import java.net.InetAddress;
import java.net.UnknownHostException;
import org.de.metux.util.StrSplit;

public class Lookup
{
    public static String getIP_str(String name)
	throws UnknownHostException
    {
//	String str = InetAddress.getByName(name).toString();
//	System.err.println("getIP_str:: "+str);
//
	return StrSplit.split(
	    InetAddress.getByName(name).toString(),
	    "/")
	    [1];
    }
}
