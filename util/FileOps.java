
package org.de.metux.util;

import java.util.Properties;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.InputStreamReader;
import java.io.File;

public class FileOps
{
    // FIXME: parse the modes
    public static boolean chmod(String fn, String mode)
    {
	return Exec.exec_command(
	    "chmod "+
	    ShellEscape.filename(mode)+
	    " "+
	    ShellEscape.filename(fn)
	);
    }

    public static boolean cp(String source, String dest)
    {
	return cp(source,dest,null);
    }
    
    public static boolean cp(String source, String dest, String mode)
    {
	if (dest.endsWith("/"))
	    mkdir(dest);
	else
	    mkdir(PathNormalizer.dirname(dest));

	if (!Exec.exec_command(
	    "cp "+
	    ShellEscape.filename(source)+
	    " "+
	    ShellEscape.filename(dest)
	))
	    return false;

	if (!StrUtil.isEmpty(mode))
	{
	    String dir;
	
	    if (new File(dest).isDirectory())
		dir = dest+"/"+PathNormalizer.basename(source);
	    else
		dir = dest;

	    return chmod(dir,mode);
	}

	return true;
    }

    public static boolean mkdir(String dir)
    {
	if ((dir==null)||(dir.length()==0))
	    return false;

	File handle = new File(dir);
	handle.mkdirs();
	return handle.isDirectory();
    }

    public static boolean mkdir(String dirs[])
    {
	if ((dirs==null)||(dirs.length==0))
	    return false;
	for (int x=0; x<dirs.length; x++)
	    mkdir(dirs[x]);
	return true;
    }

    public static void symlink(String target, String name)
    {
//	System.err.println("TARGET="+target);
//	System.err.println("NAME="+name);
	
	String cmd = 
	    "ln -s "+
	    ShellEscape.filename(target)+
	    " "+
	    ShellEscape.filename(name);
	System.err.println("symlink# "+cmd);	    
	new Exec().run(cmd);
    }
    
    public static void rm(String fn)
    {
	new File(fn).delete();
    }
    
    public static String getcwd()
    {
	File f = new File(".");
	String filePath = f.getAbsolutePath();
	int dotAt = filePath.lastIndexOf(".");
	return (new String(filePath.substring(0,dotAt)));
    }
    
    public static File getcwd_handle()
    {
	return new File(getcwd());
    }
    
    public static boolean chdir(String dirname)
    {
	File d = new File(dirname);
	if (!d.isDirectory())
	    return false;
	    
	System.setProperty("user.dir", d.getAbsolutePath());
	return true;
    }

    public static String md5(File f)
    {
	String out = _run_cmd("md5sum "+f.getAbsolutePath()+" 2>&1");
	if (out==null)
	    throw new RuntimeException("md5: exec didnt give result for: "+f);
	    
	String s[] = StrSplit.split(out);

	if (s.length==0)
	    throw new RuntimeException("md5: got no result for: "+f);

	return s[0];
    }

    static private String _run_cmd(String cmdline)
    {
	String cmds[] = cmds = new String[3];
	Process process;
	
	cmds[0] = "/bin/bash";
	cmds[1] = "-c";
	cmds[2] = cmdline+" 2>&1";
	
//	System.err.println("_run_cmd(): "+cmdline);

	try 
	{
	    process = Runtime.getRuntime().exec
	    (
		cmds,
		null,
		new File(System.getProperty("user.dir"))
	    );
	    
	    BufferedReader stdout = new BufferedReader(new InputStreamReader(process.getInputStream()));
	    String res = "";
	    String line;

	    while ((line=stdout.readLine())!=null)
		res += line;
		
	    return res;
	}
	catch (IOException e)
	{
	    throw new RuntimeException(e);
	}

//	catch (InterruptedException e)
//	{
//	    callback.error("InterruptedException in Stage::exec()"+e);
//	    return false;
//	}
    }
}
