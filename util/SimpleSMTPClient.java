
package org.de.metux.util;

import java.io.*;
import java.net.*;

public class SimpleSMTPClient
{
    public boolean debug = false;

    String to;
    String from;
    String subject;
    String body;

    java.io.BufferedReader _in;
    java.io.PrintWriter _out;
    
    String smtp_hostname;
    int smtp_port = 25;

    PrintStream out;
    
    public SimpleSMTPClient()
    {
    }
    
    public String format_addr(String addr, String name)
    {
	return name+" <"+addr+">";
    }
    
    public void setTo(String addr)
    {
	to = addr;
    }
    public void setTo(String addr, String name)
    {
	to = format_addr(addr,name);
    }
    public void setFrom(String str)
    {
	from = str;
    }
    public void setFrom(String addr, String name)
    {
	from = format_addr(addr,name);
    }
    public void setSubject(String text)
    {
	subject = text;
    }
    public void setBody(String text)
    {
	body = text;
    }

    private void send_line(String s) throws java.io.IOException 
    {
	// Send the SMTP command
	if (s != null) 
	{
	    if (debug)
		System.out.println("C: "+s);
	    _out.println(s);
	    _out.flush();
	}

	// Wait for the response
	String line = _in.readLine();
	if (line != null)
	    if (debug)
    	        System.out.println("S:" + line);
    }

    // fixme: we should have some error reporting
    public void send()
	throws java.io.IOException
    {
	if (to==null)
	    throw new RuntimeException("missing to: header");
	if (from==null)
	    throw new RuntimeException("missing from: header");
	if (subject==null)
	    throw new RuntimeException("missing subject: header");
	if (body==null)
	    throw new RuntimeException("missing body");

	// if none given, either fallback to $SMTP_SERVER or localhost
	if ((smtp_hostname==null)||smtp_hostname.length()==0)
	    smtp_hostname = new Environment().getProperty("SMTP_SERVER");
	if ((smtp_hostname==null)||smtp_hostname.length()==0)
	    smtp_hostname = "localhost";
	    
	try
	{
	    java.net.Socket s = new java.net.Socket(smtp_hostname, smtp_port);
    	    _out = new java.io.PrintWriter(s.getOutputStream());
    	    _in = new java.io.BufferedReader(
		new java.io.InputStreamReader(s.getInputStream()));

    	    send_line(null);
    	    send_line("HELO " + java.net.InetAddress.getLocalHost().getHostName());
    	    send_line("MAIL FROM: " + from);
    	    send_line("RCPT TO: " + to);
    	    send_line("DATA");
    	    _out.println("Subject:" + subject);
    	    _out.println(body);
    	    send_line(".");
    	    s.close();

	} 
	catch (Exception e) 
	{
    	    System.err.println("Error: " + e);
	}
    }
}
