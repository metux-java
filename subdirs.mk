
all:
	for i in $(SUBDIRS); do make -C $$i || exit 1; done

clean:
	for i in $(SUBDIRS); do make -C $$i clean ; done
