
package org.de.metux.util;

import java.io.File;
import java.io.IOException;

public class PathNormalize
{
    public static String normalize(String path)
    {
	if (path==null)
	    return "";
	    
	path = StrReplace.replace("//", "/", 
	       StrReplace.replace("/./", "/", path)); 

	if (path.endsWith("/"))
	    return path.substring(0,path.length()-1);
	else
	    return path;
    }
    
    // normalize and strip off sysroot
    public static String normalize_strip_sysroot(String path, String sysroot)
    {
	sysroot = normalize(sysroot);
	path    = normalize(path);
	
	if (path.startsWith(sysroot))
	    return path.substring(sysroot.length());
	else
	    return path;
    }

    public static String normalize_add_sysroot(String path, String sysroot)
    {
	sysroot = normalize(sysroot);	
	path    = normalize(path);

	if (path.startsWith("/") && (!path.startsWith(sysroot)))
	    return normalize(sysroot+"/"+path);
	else
	    return path;
    }
    
    public static String strip_cwd(String path)
    {
	try
	{
	    return strip_cwd(path,new File(".").getCanonicalPath());    
	}
	catch (IOException e)
	{
	    throw new RuntimeException("File() failed ",e);
	}
    }
    
    public static String strip_cwd(String path, String cwd)
    {
	int pos;
	
	path = normalize(path);
	cwd  = normalize(cwd);

	System.err.println("==>strip_cwd: path=\""+path+"\"");
	System.err.println("==>            cwd=\""+cwd+"\"");
	
	if ((pos=path.indexOf(cwd))==0)
	    return normalize("./"+path.substring(cwd.length()));
	else
	    return path;
    }
}
