/*

    version foo
*/

package org.de.metux.util;

import java.io.File;

public class rm
{
    public static boolean remove_recursive(String path)
    {
	return remove_recursive(new File(path));
    }
    
    public static boolean remove_recursive(File dirPath)
    {
	String [] ls = dirPath.list();

	if (ls!=null)
	{
	    for (int idx=0; idx<ls.length; idx++)
	    {
		File file = new File(dirPath, ls[idx]);
	        if (file==null)
	    	    throw new RuntimeException("rm::remove_recursive() cannot open file object for "+
			dirPath+"=>"+ls[idx]);

		if (file.isDirectory())
		    remove_recursive(file);
		file.delete();
	    }
	}
	
	dirPath.delete();

	// FIXME!!!
	return true;
    }
}
