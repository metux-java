
package org.de.metux.ip;

import org.de.metux.util.Exec;
import org.de.metux.util.ShellEscape;

public class Ping
{
    String  hostname;
    int     max = 5;
    Exec    exec;
    boolean alive;

    private class callback implements Exec.IExecCallback
    {
	public void OutputLine(String line)
	{
	    line = line.trim();

	    if (line.startsWith("PING "))
	    {
		line = line.substring(5);
//		System.err.println(" --> PING HEAD: "+line);
		int x = line.indexOf(" ");
	    }
	    else if (line.indexOf(" octets from ")>0)
	    {
//		System.out.println("PING REPLY: "+line);
		alive = true;
		exec.abort();
	    }
	    else if (line.indexOf("ping statistics")>0);
	    else if (line.indexOf("packets transmitted")>0);
	    else if (line.equals(""));
	    else
	        System.out.println(">>"+line);
	}
	
	public void ErrorLine(String line)
	{
	    System.err.println("!> "+line);
	}
	
	public void debug(String line)
	{
	}
	
	public void error(String line)
	{
	    System.err.println("EXEC ERROR: "+line);
	}
    }

    public Ping(String my_hostname)
    {
	hostname = my_hostname;
    }

    public boolean tryHostAlive()
    {
	exec = new Exec(new callback());
	exec.run("ping -c "+max+" "+ShellEscape.quoting(hostname));
	return alive;
    }
}
