
package org.de.metux.util;

import java.util.Hashtable;
import java.util.Properties;
import java.io.*;
import java.util.Enumeration;

public class TextTable
{
    public static Hashtable Load(String filename,String pkey)
    {
	Hashtable h = new Hashtable ();
	if (LoadIntoHashtable(filename,h, pkey))
	    return h;
	else
	    return null;
    }
    
    public static boolean LoadIntoHashtable(String filename, Hashtable table, String pkey)
    {
	BufferedReader in;
	
	try
	{
	    in = new BufferedReader(new FileReader(filename));
	}
	catch (FileNotFoundException e)
	{
	    return false;
	}

	try
	{
	    String line;
	    Hashtable record = new Properties();
	    int linecnt = 0;
	    
	    while ((line=in.readLine())!=null)
	    {
		int x;
		int len;
		
		/* strip off comments */
		if ((x=line.indexOf('#'))>=0)
		    line = line.substring(0,x);
		
		line = line.trim();
		if ((len=line.length())>0)
		{
		    /* -- split into key and value -- */
		    if ((x = line.indexOf(':'))>0)
	    	    {
			String key;
			String value;
			String str;
			
			key = (line.substring(0,x)).trim();
			value = (line.substring(x+1)).trim();
		    
			if (record.containsKey(key))
			    record.put(key,record.get(key)+"\n"+value);
			else
			    record.put(key,value);
		    }
		    else if ((line.indexOf("--")==0)&&(!record.isEmpty()))
		    {
			String k = (String)record.get(pkey);
			if (k.length()>0)
			    table.put(k,record);
			else
			    table.put(record,record);

			record = new Hashtable();
		    }
		    else
		        System.err.println(
			    "CORRUPT LINE ("+
			    filename+":"+linecnt+"): "+line);
		}
		linecnt++;
	    }	    
	    return true;
	}
	catch(IOException e)
	{
	    System.err.println("File input error");
	    return false;
	}
    }
    
    public static String DumpTable ( Hashtable table )
    {
	if (table==null)
	    return "<NULL>";
    
	String res = "";
	for (Enumeration e = table.keys(); e.hasMoreElements(); )
	{
	    String key = (String)e.nextElement();
	    res += "#PKEY: "+key+"\n";
	    Hashtable record = (Hashtable)table.get(key);
	    for (Enumeration re = record.keys(); re.hasMoreElements(); )
	    {
		String rk = (String)re.nextElement();
		res += rk+": "+record.get(rk)+"\n";
	    }
	    res += "-- \n";
	}
	return res;
    }
}
