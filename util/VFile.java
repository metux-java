
package org.de.metux.util;

import java.io.File;

/*
    states which the file can have:
    
    + VFile   current_root 	-> beyond which root are we working ?
    + boolean allow_breakout	-> are we allowed to break out of the root ?
    + 


*/

public class VFile
{
    String root;
    String workdir;
    String filename;
    
    public static String normalize_filename(String name)
    {
	boolean absolute = name.startsWith("/");
	boolean dirname  = name.endsWith("/");

	String splitted[] = name.split("\\/");
	String newstr[]   = new String[splitted.length];

	int y=0;
	for (int x=0; x<splitted.length; x++)
	{
	    String e = splitted[x];
	    if ((e==null)||(e.equals("")||e.equals("."))) ;
	    else if (e.equals(".."))
	    {
		if (y>0)
		    y--;
		else
		    newstr[y++] = "..";
	    }
	    else
		newstr[y++] = e;
	}
	
	if (y==0)
	    return ((absolute||dirname) ? "/" : "");
	    
	String str = (absolute ? "/" : "")+newstr[0];
	for (int x=1; x<y; x++)
	    str += "/"+newstr[x];
	
	if (dirname)
	    str += "/";

	return str;
    }
}
