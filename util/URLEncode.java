
package org.de.metux.util;

public class URLEncode
{
    public static String encode(String s)
    {
	// escape the % character at first
	s = StrReplace.replace("%","%25", s);

	// now the other chars
	s = StrReplace.replace(":", "%3A", 
	    StrReplace.replace("/", "%2F", s));
	return s;
    }

    public static String decode(String s)
    {
//	System.err.println("decoding string: "+s);
	
	return 
	    StrReplace.replace("%3A", ":",
	    StrReplace.replace("%2F", "/",    
	    StrReplace.replace("%25", "%", s)));
    }
}