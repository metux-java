
package org.de.metux.dbObjects;

public abstract class Record
{
    abstract Data oidValue();
    abstract String oidField();
    abstract String relationName();
    abstract boolean SQLData_load(DataList d);
    abstract DataList SQLData_store();
    
    public String sqlInsert()
    {
	return QueryGenerator.InsertQuery(relationName(),SQLData_store());
    }
    
    public String sqlUpdate()
    {
	return QueryGenerator.UpdateQueryAND
	(
	    relationName(),
	    oidField(),
	    oidValue(),
	    SQLData_store()
	);
    }
}
