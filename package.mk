
PKGDIR=$(subst .,/,$(PACKAGE))
CLASSPATH="$(BUILDDIR):$(CLASSPATH_ADD):."
CLASS_SOURCES=$(shell for i in $(CLASSES); do echo $$i.java; done)
REAL_CLASS_FILES=$(shell for i in $(CLASSES); do echo $(BUILDDIR)/$(PKGDIR)/$$i.class; done)
REAL_OBJ_FILES=$(shell for i in $(CLASSES); do echo $(BUILDDIR)/$(PKGDIR)/$$i.o; done)
XXX_CLASS_FILES=$(shell for i in $(CLASSES); do echo $(PKGDIR)/$$i.class; done)
XXX_OBJ_FILES=$(shell for i in $(CLASSES); do echo $(PKGDIR)/$$i.o; done)
IMPORT_FILES=$(shell for i in $(IMPORTS); do echo $(BUILDDIR)/$$i.jar; done)
#CMD_JAVAC=gcj -C
CMD_JAVAC=javac
CMD_JAVAR=java

all:	prepare $(REAL_CLASS_FILES) run-unit-test

.SUFFIXES:	.java	.class

$(BUILDDIR)$(PACKAGE).jar: $(REAL_CLASS_FILES)
	cd $(BUILDDIR)/ && jar cvf $(PACKAGE).jar $(XXX_CLASS_FILES)
	
prepare:
	mkdir -p $(BUILDDIR)/$(PKGDIR)

$(BUILDDIR)/$(PKGDIR)/%.o:	%.java
	gcj -c $<
	mv *.o $(BUILDDIR)/$(PKGDIR)

$(BUILDDIR)/$(PKGDIR)/_unit_test.class:	_unit_test.java
	CLASSPATH=$(CLASSPATH) $(CMD_JAVAC) $<

$(BUILDDIR)/$(PKGDIR)/%.class:	%.java
	CLASSPATH=$(CLASSPATH) $(CMD_JAVAC) $<
	echo Moving `basename $@` to $(BUILDDIR)/$(PKGDIR)
	mv `basename $@` $(BUILDDIR)/$(PKGDIR) 
	echo "Processing subclasses: $(SUBCLASSES)"
	for i in $(SUBCLASSES) ; do test -f "$$i.class" && mv "$$i.class" $(BUILDDIR)/$(PKGDIR) ; done || true

run-unit-test:		_unit_test.java
	CLASSPATH=$(CLASSPATH) $(CMD_JAVAC) _unit_test.java
	CLASSPATH=$(CLASSPATH) $(CMD_JAVAR) _unit_test

clean:
	rm -f *.o *.class run-unit-test $(REAL_CLASS_FILES) $(REAL_OBJ_FILES) $(BUILDDIR)$(PACKAGE).jar
