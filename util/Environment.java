
package org.de.metux.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Properties;
import java.io.InputStreamReader;

/* 
   grabbed this code somewhere on the web

   we need it since stupid folks @ sun tend to remove essential functions 
   from java-spec and other stupid folks on various vm projects had
   to follow them.
   
   this class is a really ugly hack! 
   imagine: we have to spawn another process just for retrieving 
   the env of this process.
   against the wall w/ the dude who decided to remove System.getenv()
*/
   
public class Environment extends Properties
{
    public Environment()
    {
	super();
	String cmd;
	String os = System.getProperty("os.name").toLowerCase();
	
	if (os.indexOf("windows")!=-1)
	    cmd = ((os.indexOf("95")!=-1 || 
	           (os.indexOf("98")!=-1) ||
		   (os.indexOf("ME")!=-1)) ? "command.com":"cmd")+" /Cset";
	else
	    cmd = "env";
	
	try
	{
	    Process p = Runtime.getRuntime().exec(cmd);
	    load(p.getInputStream());
	    BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
	    for (String line=""; (line=br.readLine())!=null;)
	    {
		int pos = line.indexOf("=");
	        setProperty(line.substring(0,pos),line.substring(pos+1));
	    }
	    br.close();
	}
	catch (IOException e)
	{
	    setProperty("__EXCEPTION", e.toString());
	}
    }
    
    static Environment env;
    
    public static String getenv(String name)
    {
	if (env==null)
	    env = new Environment();
	return env.getProperty(name);
    }
    
    public static String getenv(String name, String def)
    {
	String s = getenv(name);
	if (s == null)
	    return def;
	return s;
    }
    
    public static boolean getenv_bool(String name, boolean def)
    {
	String val = getenv(name,null);
	if (val==null)
	    return def;
	val = val.toLowerCase();
	
	if (val.equals("on")||val.equals("yes")||val.equals("true")||val.equals("1"))
	    return true;
	if (val.equals("off")||val.equals("no")||val.equals("false")||val.equals("0"))
	    return false;
	
	throw new RuntimeException("getenv_bool(): could not decode \""+val+"\"");
    }
}
