
package org.de.metux.briegel.stages;

import org.de.metux.briegel.log.*;
import org.de.metux.briegel.conf.*;
import java.io.*;
import java.net.*;

public class FetchSource extends Stage
{
    protected String filename;
    protected String url;
    protected File   file_handle;
    protected URL    url_handle;
    
    public FetchSource(BriegelConf cf)
    {
	super(cf.logger.FETCHSOURCE,cf);
    }

    boolean init()
    {
	filename = config.cf_get_str("source-file");
	url      = config.cf_get_str("fetch-source-url");

	notice("source file: "+filename);
	notice("source url:  "+url);

	/* check if the source file name is okay ... necessary ? */
	if (filename.length()<3)
	{
	    error("Source filename invalid: "+filename);
	    return false;
	}
	file_handle = new File (filename);

	return true;
    }
    
    boolean check_exists()
    {
	if (file_handle.exists())
	{
	    notice("Source file already exists. No fetch necessary.");
	    return true;
	}
	return false;
    }
    
    /* dirty hack: uses external curl */
    boolean download()
    {
	URLDownloader dn = new URLDownloader();
	return dn.download(url,filename);
    }
    
    /* doenst work properly :/ */
    boolean download_x()
    {
	try
	{
	    url_handle = new URL(url);
	}
	catch (MalformedURLException e)
	{
	    error("Malformed URL: "+e);
	    return false;
	}

	URLConnection conn;
	try
	{
	    conn = url_handle.openConnection();    
	}
	catch (IOException e)
	{
	    error("download: Could not open URL: "+e);
	    return false;
	}

	notice("Download size: "+conn.getContentLength());
	
	try 
	{
	    InputStream in = conn.getInputStream();
	}
	catch (IOException e)
	{
	    error("download: could not open URL for reading: "+e);
	}
	    
	OutputStream out;
	
	try
	{ out = new FileOutputStream(filename); }
	catch (IOException e)
	{
	    error("download: Could not open local file: "+e);
	    return false;
	}
	
	return false;
    }
    
        
    boolean run_stage()
    {
	if (!init())
	    return false;

	if (check_exists())
	    return true;
	    
	/* if not yet existing, try to download */
	File parent = file_handle.getParentFile();
	notice("Parent dir is: "+parent.getAbsolutePath());	
	parent.mkdir();
	    
	download();
	
	return false;
    }
}
