
package org.de.metux.util;

import java.util.StringTokenizer;

public class Version
{
    public int digits[];
    public static final int WIDTH = 4;

    public Version()
    {
	digits = new int[WIDTH];
    }

    public Version(String version)
    {
	digits = new int[WIDTH];
	
	StringTokenizer tok = new StringTokenizer ( version, ".", false );
	// FIXME: should have some real error handling
	for (int x=0; (tok.hasMoreTokens() && (x<WIDTH)); x++)
	    digits[x] = Integer.parseInt(tok.nextToken());
    }

    public String toString()
    {
	String str = String.valueOf(digits[0]);
	for (int x=1; x<WIDTH; x++)
	    str += "." + digits[x];

	return str;    
    }
    
    /* compare this Version with another one */
    public int compareTo(Version ver)
    {
	for (int x=0; x<WIDTH; x++)
	{
	    if (this.digits[x] > ver.digits[x])	
		return 1;
	    if (this.digits[x] < ver.digits[x])
		return -1;
	}        
	return 0;
    }
    
    /* check if it is null */
    public boolean isNull()
    {
	for (int x=0; x<WIDTH; x++)
	    if (this.digits[x] != 0)
		return false;
	return true;
    }
}
