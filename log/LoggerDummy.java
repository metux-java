
package org.de.metux.log;

public class LoggerDummy implements ILogger
{
    final public void setLogLevel(int ll)
    {
    }

    final public void error(String stage, String message)
    {
    }
    
    final public void warning(String stage, String message)
    {
    }
     
    final public void debug(String stage, String message)
    {
    }
    
    final public void notice(String stage, String message)
    {
    }
}
