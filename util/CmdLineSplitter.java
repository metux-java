
package org.de.metux.util;

// FOO

import java.util.StringTokenizer;

/* 

    splits an command line into an array of strings.

    FIXME: currently no quotes and escapes supported.

*/

public class CmdLineSplitter
{
    public static String[] split(String str)
    {
	/* with current versions of libgcj we need to use the StringTokenizer,
	   since regex support is missing */

	str = StrReplace.replace("\\ ","++++++++++++",str).trim();
	
	if (str.length()==0)
	    return new String[0];

	String args[] = str.split(" ");
	for (int x=0; x<args.length; x++)
	    args[x] = StrReplace.replace("++++++++++++", " ", args[x]);

	return args;
    }
}
