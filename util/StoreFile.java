
package org.de.metux.util;

import java.io.IOException;
import java.io.FileWriter;
import java.io.File;

public class StoreFile
{
    public static boolean store(File file, String text)
    {
	return store(file.getAbsolutePath(),text);
    }

    public static boolean store(String filename, String text)
    {
	try
	{
	    FileWriter wr = new FileWriter(filename,false);
	    wr.write(text,0,text.length());
	    wr.flush();
	    wr.close();
	    return true;
	}
	catch (IOException e)
	{
	    return false;
	}

	
    }
    
    public static boolean store(String filename, String text, String mode)
    {
	if (!store(filename,text))
	    return false;
	
	return FileOps.chmod(filename,mode);
    }
}
    