
package org.de.metux.util;

// a simplicistic parser for shell style variable definitions
// FIXME: has to be improved

public class ShellVariableDef
{
    public String name;
    public String value;

    public boolean getBoolean ()
    {
	if ((value!=null) && (value.equals("yes")))
	    return true;
	else 
	    return false;
    }
    
    public class XEmpty extends Exception
    {
	public XEmpty(String s) { super(s); }
    }
    public class XParseFailed extends Exception
    {
	public XParseFailed(String s) { super(s); }
    }

    public int getInt()
    {
	try 
	{
	    return Integer.parseInt(value);
	}
	catch (NumberFormatException e)
	{
	    System.err.println(
		"ShellVariableDef::getInt() format error: "+
		name+"'"+value+"'");
	    throw e;
	}
    }

    public int getInt(int def)
    {
	try 
	{
	    return Integer.parseInt(value);
	}
	catch (NumberFormatException e)
	{
	    return def;
	}
    }
    
    public ShellVariableDef(String line) throws XEmpty, XParseFailed
    {
	if (line==null)
	    throw new XEmpty("null pointer assigned");
	
	line = line.trim();
	
	if (line.length()<1)
	    throw new ShellVariableDef.XEmpty("empty string (1)");
	
	// look for an # (comment)
	int comment_pos = line.indexOf('#');

	// look for an equal sign
	int equal_pos;
	if ((equal_pos=line.indexOf("='"))>0)
	{
	    if ((comment_pos>-1) && (comment_pos<equal_pos))
		throw new ShellVariableDef.XParseFailed("# before =");

	    int fin_pos = line.indexOf('\'', equal_pos+2);	
	    if (fin_pos <= -1)
		throw new ShellVariableDef.XParseFailed("missing closing \"'\"");

	    name  = line.substring(0,equal_pos).trim();
	    value = line.substring(equal_pos+2,fin_pos);
	}

	else if ((equal_pos=line.indexOf("="))>0)
	{
	    if ((comment_pos>-1) && (comment_pos<equal_pos))
		throw new ShellVariableDef.XParseFailed("# before =");

	    name  = line.substring(0,equal_pos).trim();

	    int fin_pos = line.indexOf(' ', equal_pos);

	    if (fin_pos>1)	
		value = line.substring(equal_pos+1,fin_pos);
	    else
		value = line.substring(equal_pos+1);
	}

	else
	{
	    // we have to test, whether the line is in fact empty
	    if (comment_pos==0)
		throw new ShellVariableDef.XEmpty("just comments (1)");

	    if ((comment_pos>0)&&(line.substring(0,comment_pos).trim().length()<1))
		throw new ShellVariableDef.XEmpty("just comments (2)");

	    throw new ShellVariableDef.XParseFailed("missing =");
	}

    }
}
