
import org.de.metux.util.StrReplace;
import org.de.metux.util.Environment;
//import org.de.metux.util.PathNormalize;
import org.de.metux.util.PathNormalizer;
import org.de.metux.util.Version;
import org.de.metux.util.ShellVariableDef;
import org.de.metux.util.Exec;
import org.de.metux.util.Depend;
//import org.de.metux.util.IPTools;
import org.de.metux.util.SimpleSMTPClient;
import org.de.metux.util.HTMLEntities;
import org.de.metux.util.VFile;
import org.de.metux.util.mkdir;
import org.de.metux.util.FileOps;

import java.util.*;
import java.util.regex.*;

public class _test_util
{
    static void __normalize_strip_sr(String src, String sysroot, String dest)
    {
//	String out = PathNormalize.normalize_strip_sysroot(src,sysroot);
//	if (!out.equals(dest))
//	    throw new RuntimeException(	
//		"Testing PathNormalize.normalize() failed: \n"+
//		    "source=\""+src+"\"\n"+
//		    "sysroot=\""+sysroot+"\"\n"+
//		    "out=\""+out+"\"\n"+
//		    "dest=\""+dest+"\"");
//
//	System.out.println("PathNormalize(\""+src+"\") OK");
    }

    static void test_PathNormalize_strip_sysroot()
    {
	__normalize_strip_sr(
	    "/opt/xcompiler//samma/sys-root//path/to///my/foo/./bar//././boo/",
	    "/opt//xcompiler/samma/sys-root/",
	    "/path/to/my/foo/bar/boo"
	);
	__normalize_strip_sr(
	    "to///my/foo/./bar//././boo/",
	    "/opt//xcompiler/samma//sys-root//",
	    "to/my/foo/bar/boo"
	);
	__normalize_strip_sr(
	    "//path/to///my/foo/./bar//././boo",
	    "/opt//xcompiler/samma/sys-root",
	    "/path/to/my/foo/bar/boo"
	);
	__normalize_strip_sr(
	    "//opt/xcompiler/samma/sys-root//path/to///my/foo/./bar//././boo///",
	    "/opt//xcompiler/samma/sys-root",
	    "/path/to/my/foo/bar/boo"
	);
    }


    static void __normalize_add_sr(String src, String sysroot, String dest)
    {
//	String out = PathNormalize.normalize_add_sysroot(src,sysroot);
//	if (!out.equals(dest))
//	    throw new RuntimeException(	
//		"Testing PathNormalize.normalize() failed: \n"+
//		    "source=\""+src+"\"\n"+
//		    "sysroot=\""+sysroot+"\"\n"+
//		    "out=\""+out+"\"\n"+
//		    "dest=\""+dest+"\"");
//
//	System.out.println("PathNormalize(\""+src+"\") OK");
    }

    static void test_PathNormalize_add_sysroot()
    {
	__normalize_add_sr(
	    "/opt/xcompiler//samma/sys-root//path/to///my/foo/./bar//././boo/",
	    "/opt//xcompiler/samma/sys-root/",
	    "/opt/xcompiler/samma/sys-root/path/to/my/foo/bar/boo"
	);
	__normalize_add_sr(
	    "to///my/foo/./bar//././boo/",
	    "/opt//xcompiler/samma//sys-root//",
	    "to/my/foo/bar/boo"
	);
	__normalize_add_sr(
	    "//path/to///my/foo/./bar//././boo",
	    "/opt//xcompiler/samma/sys-root",
	    "/opt/xcompiler/samma/sys-root/path/to/my/foo/bar/boo"
	);
	__normalize_add_sr(
	    "//path/to///my/foo/./bar//././boo",
	    "/opt//xcompiler/samma/sys-root//./",
	    "/opt/xcompiler/samma/sys-root/path/to/my/foo/bar/boo"
	);
	__normalize_add_sr(
	    "//opt/xcompiler/samma/sys-root//path/to///my/foo/./bar//././boo///",
	    "/opt/./xcompiler/samma/sys-root",
	    "/opt/xcompiler/samma/sys-root/path/to/my/foo/bar/boo"
	);
    }


    static void __normalize(String src, String mask)
    {
//	String out = PathNormalize.normalize(src);
//	if (!out.equals(mask))
//	    throw new RuntimeException(	
//		"Testing PathNormalize.normalize() failed with \""+
//		    src+"\" -- is \""+out+"\" -- should be \""+mask+"\"");
//
//	System.out.println("PathNormalize(\""+src+"\") OK");
    }
    
    static void __replace_suffix(String filename, String suffix, String mask)
    {
	String out = PathNormalizer.replace_suffix(filename,suffix);
	if (!out.equals(mask))
	    throw new RuntimeException(
		"Testing PathNormalizer.replace_suffix() failed with \""+
		    filename+"\" / \""+suffix+"\" -- is \""+out+"\" -- should be \""+mask+"\"");
	System.out.println("PathNormalizer.replace_suffix(\""+filename+"\",\""+suffix+"\")");
    }

    static void test_PathNormalize()
    {
	__normalize(
	    "//path/to///my/foo/./bar//././boo/",
	    "/path/to/my/foo/bar/boo"
	);
	__normalize(
	    "to///my/foo/./bar//././boo/",
	    "to/my/foo/bar/boo"
	);
	__normalize(
	    "//path/to///my/foo/./bar//././boo",
	    "/path/to/my/foo/bar/boo"
	);
	__normalize(
	    "//path/to///my/foo/./bar//././boo///",
	    "/path/to/my/foo/bar/boo"
	);
	__replace_suffix(
	    "//path/to/my/foo.bar.boo",
	    "c",
	    "//path/to/my/foo.bar.c"
	);
	__replace_suffix(
	    "//path/to/my/foo.c",
	    "c",
	    "//path/to/my/foo.c"
	);
	__replace_suffix(
	    "//path/for/dummy.obj",
	    "in",
	    "//path/for/dummy.in"
	);
	__replace_suffix(
	    "huhu/allo",
	    "asm",
	    "huhu/allo.asm"
	);
    }

    static void test_Java_Regex()
    {
	if (!Pattern.matches("a*b", "aaaaab"))
	    throw new RuntimeException("regex test FAILED");    

        System.out.println("regex test OK");
    }
    
    
    /* testing StrReplace::replace */
    static void test_StrReplace_1()
    {
	String knollo = "HELLO WORLD{TERM}";
	int pos = knollo.indexOf("{TERM}");
	
	if (pos != 11)
	{
	    System.err.println("java.lang.String class buggy: indexOf() doesnt find SUFFIX");
	    System.exit(1);
	}
    }
    
    static boolean test_StrReplace_try(
	String name,
	String source, 
	String needle, 
	String replace, 
	String ok)
    {
	String res = 
	    StrReplace.replace(needle, replace, source);
	
	if (res.equals(ok))
	{
	    System.out.println("StrReplace test "+name+" ... OK");
	    return true;
	}
	else
	{
	    System.err.println("StrReplace test "+name+" ... FAILED !");
	    return false;
	}
    }
    
    static void test_StrReplace()
    {
	boolean okay;
	
	okay = 
	(
	    test_StrReplace_try(
		"endswith",
		"bummsfall&quot;",
		"&quot;",
		"era666",
		"bummsfallera666"
	    )				&&
	    test_StrReplace_try(
		"multi-replace",
		"//hello/world/////baa/",
		"//",
		"/",
		"/hello/world/baa/"
	    )				&&
	    test_StrReplace_try(
		"simple-replace",
		"/install/config/styles/{STYLE}.cf{TERM_NORMAL}",
		"{TERM_NORMAL}",
		"FOO",
		"/install/config/styles/{STYLE}.cfFOO"
	    )				&&
	    test_StrReplace_try(
		"simple-replace",
		"/install/config/styles/{STYLE}.cf",
		"{STYLE}",
		"system/homer",
		"/install/config/styles/system/homer.cf"
	    )
	);
	
	if (!okay)
	    System.exit(1);

        System.out.println("selftest: StrReplace.replace() ... passed");
    }
    
    static public void test_Env()
    {
	Environment env = new Environment();
	System.out.println("ENV: HOME="+env.getProperty("HOME"));
	System.out.println("ENV: LOGNAME="+env.getProperty("LOGNAME"));
	System.out.println("ENV: LS_COLORS="+env.getProperty("LS_COLORS"));
    }

    static public void test_Env_native()
    {
	System.out.println("sysENV: HOME="+System.getenv("HOME"));
	System.out.println("sysENV: LOGNAME="+System.getenv("LOGNAME"));
	System.out.println("sysENV: LS_COLORS="+System.getenv("LS_COLORS"));
    }

    static public void test_Version()
    {
	Version ver = new Version ( "1.2.3.4" );
	
	if ((ver.digits[0]!=1)||(ver.digits[1]!=2)||
	    (ver.digits[2]!=3)||(ver.digits[3]!=4))
	{
	    System.err.println("Version class failed test #1");
	    System.exit(1);
	}
	
	System.out.println("toString() => "+ver.toString());
	
	if (!ver.toString().equals("1.2.3.4"))
	{
	    System.err.println("Version class failed test #2");
	    System.exit(1);
	}
    
	System.out.println("Version class test passed.");
    }    

    static void test_shellvariabledef() 
	throws ShellVariableDef.XParseFailed, ShellVariableDef.XEmpty
    {
	// test several cases for XEmpty
	try {  
	    new ShellVariableDef(null); 
	    throw new RuntimeException("error in ShellVariablesDef: calling with null should throw XEmpty");
	}
	catch (ShellVariableDef.XEmpty e)
	{
	    System.out.println("ShellVariableDef: case 1 okay");
	}
	catch (ShellVariableDef.XParseFailed e)
	{
	    throw new RuntimeException("error in ShellVariablesDef: calling with null string brought parse error: "+e);
	}

	// test several cases for XEmpty
	try {  
	    new ShellVariableDef("  # adsfaasdf "); 
	    throw new RuntimeException("error in ShellVariablesDef: calling with empty  shuld throw XEmpty");
	}
	catch (ShellVariableDef.XEmpty e)
	{
	    System.out.println("ShellVariableDef: case 2 okay");
	}
	catch (ShellVariableDef.XParseFailed e)
	{
	    throw new RuntimeException("error in ShellVariablesDef: calling with empty string brought parse error: "+e);
	}

	// try a fully conform string 
	try {
	    ShellVariableDef shvar = 
		new ShellVariableDef("foo='furunkel' # hello world");
	    if (shvar.name==null)
		throw new RuntimeException("case 3 error: name is null");
	    if (shvar.value==null)
		throw new RuntimeException("case 3 error: value is null");
	    if (!shvar.name.equals("foo"))
		throw new RuntimeException("case 3 error: name is not \"foo\" -- \""+shvar.name+"\"");
	    if (!shvar.value.equals("furunkel"))
		throw new RuntimeException("case 3 error: value is not \"furunkel\" -- \""+shvar.value+"\"");
	}
	catch (ShellVariableDef.XParseFailed e)
	{
	    throw new RuntimeException("case 3 error: parse failed: "+e);
	}
	catch (ShellVariableDef.XEmpty e)
	{
	    throw new RuntimeException("case 3 error: parse failed: "+e);
	}

	// try a string without quotes (ie. numeric or boolean
	try {
	    ShellVariableDef shvar = 
		new ShellVariableDef("foo=yes # hello world");
	    if (shvar.name==null)
		throw new RuntimeException("case 4 error: name is null");
	    if (shvar.value==null)
		throw new RuntimeException("case 4 error: value is null");
	    if (!shvar.name.equals("foo"))
		throw new RuntimeException("case 4 error: name is not \"foo\" -- \""+shvar.name+"\"");
	    if (!shvar.value.equals("yes"))
		throw new RuntimeException("case 4 error: value is not \"yes\" -- \""+shvar.value+"\"");
	}
	catch (ShellVariableDef.XParseFailed e)
	{
	    throw new RuntimeException("case 4 error: parse failed: "+e);
	}
	catch (ShellVariableDef.XEmpty e)
	{
	    throw new RuntimeException("case 4 error: parse failed: "+e);
	}

	System.out.println("ShellVariableDef passed cases 1..4");
    }
    
    static void test_Exec()
    {
	Exec.exec_command("ls /opt");
    }

    static void test_Depend()
    {
	Depend dep;
	
	/* --- test case 1 --- */
	dep = new Depend("knollo[foo][bar][blubb]");
	if (!(dep.package_name.equals("knollo")))
	    throw new RuntimeException("Depend{} case 1 failed (pkgname)");

	if (dep.features==null)
	    throw new RuntimeException("Depend{} case 1 failed. no features");

	System.out.println("NUM_FEATURES: "+dep.features.length);
	System.out.println("FEAT0: "+dep.features[0]);
	System.out.println("FEAT1: "+dep.features[1]);
	System.out.println("FEAT2: "+dep.features[2]);
	
	if (!(
	    (dep.features.length==3)		&&
	    dep.features[0].equals("foo") 	&& 
	    dep.features[1].equals("bar") 	&&
	    dep.features[2].equals("blubb")
	))
	    throw new RuntimeException("Depend{} case 1 failed (features)");
	    
	/* --- test case 2 --- */
	
	dep = new Depend("furunkel(>=1.2.3.0)");
	if (!dep.package_name.equals("furunkel"))
	    throw new RuntimeException("Depend{} case 2 failed (name) \""+dep.package_name+"\"");
	    
	if (!dep.version.toString().equals("1.2.3.0"))
	    throw new RuntimeException("Depend{} case 2 failed (version)");
	    
	/* --- test case 3 --- */
	dep = new Depend("albert(>=7.1.6.3)[foo][bratzel]");
	if ((!dep.package_name.equals("albert")) || 
	    (!dep.version.toString().equals("7.1.6.3")) ||
	    (dep.features.length!=2) ||
	    (!dep.features[0].equals("foo")) ||
	    (!dep.features[1].equals("bratzel"))
	)
	    throw new RuntimeException("Depend{} case 3 failed" );

	/* --- test case 4 --- */
	dep = new Depend("hutzbutz");
	if ((!dep.package_name.equals("hutzbutz")) ||
	    (!dep.version.toString().equals("0.0.0.0")) ||
	    (dep.features != null))
	    throw new RuntimeException("Depend{} case 4 failed");
    }

    static public void test_basename()
    {
	System.err.println("Testing PathNormalizer::basename()");
	
	String a = PathNormalizer.basename("test123.so");
	String b = PathNormalizer.basename("/foo/bar.txt");
	String c = PathNormalizer.basename("/knollo//");

	if (!a.equals("test123.so"))	
	    throw new RuntimeException("test basename() #1 failed");
	if (!b.equals("bar.txt"))
	    throw new RuntimeException("test basename() #2 failed");
	if (!c.equals(""))
	    throw new RuntimeException("test basename() #3 failed");
    }

    static public void test_dirname()
    {
	System.out.println("Testing PathNormalizer::dirname()");

	String dirname1 = PathNormalizer.dirname("/foo/bar/boo");
	String dirname2 = PathNormalizer.dirname("bumm.bumm.blahh");
	String dirname3 = PathNormalizer.dirname("");
	String dirname4 = PathNormalizer.dirname(null);
	String dirname5 = PathNormalizer.dirname("test/knollo.la");
	
	if (!dirname1.equals("/foo/bar"))
	    throw new RuntimeException("test_dirname() #1 failed \""+dirname1+"\"");
	if (!dirname2.equals(""))
	    throw new RuntimeException("test_dirname() #2 failed \""+dirname2+"\"");
	if (!dirname3.equals(""))
	    throw new RuntimeException("test_dirname() #3 failed \""+dirname3+"\"");
	if (!dirname4.equals(""))
	    throw new RuntimeException("test_dirname() #4 failed \""+dirname4+"\"");
	if (!dirname5.equals("test"))
	    throw new RuntimeException("test_dirname() #5 failed \""+dirname5+"\"");
    }

    static public void test_filename_split()
    {
	String [] res;
	
	res = PathNormalizer.split_filename("one.c");
	if (res.length != 2)
	    throw new RuntimeException("test_filename_split() #1.0 failed: return size should be 2, not "+res.length);
	if (!res[0].equals("one"))
	    throw new RuntimeException("test_filename_split() #1.1 failed: \""+res[0]+"\"");
	if (!res[1].equals(".c"))
	    throw new RuntimeException("test_filename_split() #1.2 failed: \""+res[1]+"\"");

	res = PathNormalizer.split_filename("boo");
	if (res.length != 2)
	    throw new RuntimeException("test_filename_split() #2.0 failed: return size should be 2, not "+res.length);
	if (!res[0].equals("boo"))
	    throw new RuntimeException("test_filename_split() #2.2 failed: \""+res[0]+"\"");
	if (!res[1].equals(""))
	    throw new RuntimeException("test_filename_split() #2.3 failed: \""+res[1]+"\"");

	res = PathNormalizer.split_filename("aha.");
	if (res.length != 2)
	    throw new RuntimeException("test_filename_split() #3.0 failed: return size should be 2, not "+res.length);
	if (!res[0].equals("aha"))
	    throw new RuntimeException("test_filename_split() #3.2 failed: \""+res[0]+"\"");
	if (!res[1].equals("."))
	    throw new RuntimeException("test_filename_split() #3.3 failed: \""+res[1]+"\"");
    }
                
    static public void test_SimpleSMTPClient()
	throws Exception
    {
	System.out.println("Testing mail ....");
	SimpleSMTPClient post = new SimpleSMTPClient();
	post.setTo("weigelt@metux.de");
	post.setFrom("classlib@metux.de");
	post.setSubject("classlib regress test");
	post.setBody("Hello World huhuh");
	post.send();
	System.out.println("Done");
    }
    
    static public void test_HTMLEntities()
    {
	String res = HTMLEntities.decode("test 213 &quot;");
	
	if (res.equals("test 213 \""))
	{
	    System.err.println("HTMLEntities.decode() OK");
	}
	else
	{
	    System.err.println("HTMLEntities.decode() FAILED");
	    System.err.println(" res=\""+res+"\"");
	    System.exit(1);
	}
    }

    static public void test_VFile_normalize_filename(String test, String okay)
    {
	System.err.println("Testing VFile.normalize_filename(\""+test+"\")");
	String out = VFile.normalize_filename(test);
	if (out.equals(okay))
	    System.err.println(" --> OK");
	else
	    throw new RuntimeException("VFile_normalize_filename failed test: out=\""+out+"\" okay=\""+okay+"\"");
    }
    
    static public void test_VFile()
	throws Exception
    {
	test_VFile_normalize_filename("etc//./passwd", "etc/passwd");
	test_VFile_normalize_filename("/etc///passwd/", "/etc/passwd/");
	test_VFile_normalize_filename("/../etc//passwd", "/../etc/passwd");
	test_VFile_normalize_filename("/etc/../passwd", "/passwd");
	test_VFile_normalize_filename("etc/../passwd", "passwd");
    }

    static public void main(String args[]) throws Exception
    {
	test_StrReplace();
	test_basename();
	test_dirname();
	test_PathNormalize();
	test_PathNormalize_strip_sysroot();
	test_PathNormalize_add_sysroot();
	test_Version();
	test_Exec();
	test_Env_native();	
	test_Java_Regex();
	test_shellvariabledef();
	test_filename_split();

	// test IPTool.getInterfaceAddress()
//	IPTools.getInterfaceAddress("lo");
	
	test_Depend();
	test_SimpleSMTPClient();
	test_HTMLEntities();
	
	System.err.println("CWD="+FileOps.getcwd());
	
	test_VFile();
	
//	mkdir.mkdir_recursive("/home/briegel/download/zlib/huhu/123/");
    }
}
