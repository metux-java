
package org.de.metux.sysinf;

import org.de.metux.util.Exec;
import org.de.metux.util.StrSplit;

public class FilesystemUsage
{
    public static final String df_command = "df -P -T";
    
    // FIXME: add filters for several filesystem time    
    public static FilesystemUsageEntry[] getFilesystemUsage()
    {
	String [] lines = new Exec().run_catch(df_command,true).split("\n");
	FilesystemUsageEntry[] entries = new FilesystemUsageEntry[lines.length-1];
	
	for (int x=1; x<lines.length; x++)
	{
	    String tabs []  = lines[x].split("\\s+");
	    FilesystemUsageEntry e = new FilesystemUsageEntry();
	    e.filesystem  = tabs[0];
	    e.type        = tabs[1];
	    e.space_total = Integer.parseInt(tabs[2]);
	    e.space_free  = Integer.parseInt(tabs[3]);
	    e.space_used  = Integer.parseInt(tabs[4]);
	    e.usage_rate  = Integer.parseInt(tabs[5].substring(0,tabs[5].length()-1));
	    e.mountpoint  = tabs[6];
	    e.blocksize   = 1024;
	    entries[x-1] = e;
	}
	
	return entries;
    }
}
