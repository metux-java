
//
//  FIXME: replace Hashtable by Properties
//

package org.de.metux.propertylist;

import java.io.*;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

import java.lang.NumberFormatException;

import org.de.metux.util.StrSplit;
import org.de.metux.util.TextDB;
import org.de.metux.util.UniqueNameList;
import org.de.metux.util.LoadFile;
import org.de.metux.util.StrUtil;
import org.de.metux.util.CachedDatasource;

import java.net.URLEncoder;

import java.net.URL;

public class Propertylist implements IPropertylist
{
    Properties db = new Properties();
    Properties cache = new Properties();
    IPostprocessor postprocessor;

    public static Hashtable db_recycling = null;
    public static CachedDatasource datasource_cache = new CachedDatasource();

    private class dbcache_ent
    {
	public Properties proplist;
	public String text;
    }

    /* this method loads data from a hashtable into the propertysheet
       with low priority, means that existing fields are not touched.

       the opposite would be high priority meaning existing fields 
       will be overwritten.
    */

    public void setPostprocessor(IPostprocessor p)
    {
	postprocessor = p;
    }
    
    public void runPostprocessor()
	throws EIllegalValue
    {
	if (postprocessor==null)
	    return;
	
	/* run as long as it returns true */
	while(postprocessor.propertylist_postprocess((IPropertylist)this));
    }

    /* get a really raw string, directly from database */
    public String db_get(String field)
    {
	return (String)db.get(field);
    }

    public void loadProperties_sub(Properties pr)
    {
	for (Enumeration e = pr.keys(); e.hasMoreElements(); )
	{
	    String key = (String) e.nextElement();

	    /* fields with !! always have precedence */
	    if (key.startsWith("!!"))
		db.put(key.substring(2),(String)pr.get(key));

	    /* fields with ++ are always added, not just overwritten */
	    else if (key.startsWith("++"))
		add(key.substring(2),(String)pr.get(key));

	    /* in this loading mode we do not overwrite existing fields */
	    else if (!db.containsKey(key))
		set(key,(String)pr.get(key));
	}
	cache.clear();
    }

    public void loadProperties_top(Properties pr)
    {
	for (Enumeration e = pr.keys(); e.hasMoreElements(); )
	{
	    String key = (String) e.nextElement();
	    if (key.startsWith("!!"))
		set(key.substring(2),(String)pr.get(key));
	    else if (key.startsWith("++"))
		add(key.substring(2),(String)pr.get(key));
	    else
		set(key,(String)pr.get(key));
	}
    }

    public void remove ( String key )
    {
	db.remove(key);
	cache.clear();
    }
        
    public void set ( String key, String value )
    {
	db.put(key,value);
	cache.clear();
    }

    public void add ( String key, String value )
    {
	String got;
	if ((key==null)||(value==null)) return;

	if ((got=db_get(key))!=null)
	    db.put(key,got+"\n"+value);
	else
	    db.put(key,value);
    }

    // can be overwritten, ie for namespace handling, hierachies, etc
    public String get_raw(String name)
	throws EIllegalValue
    {
	if (name.startsWith(":URLENCODE:"))
	{
	    name = name.substring(11);
	    return URLEncoder.encode(get_str(name));
	}

	return db_get(name);
    }
    
    public String get_str(String name)
	throws EIllegalValue
    {
	return get_str_rec(name,100);	    
    }

    public String[] get_list(String name)
	throws EIllegalValue
    {
	return StrSplit.split(get_str(name));
    }
    
    /* can be overwritten in order to handle special things like exec() 
       returns null if nothing happened */
    public String handle_special_str(String key)
    {
	return null;
    }

    public boolean get_bool(String name, boolean def)
    {
	try
	{
	    return get_bool(name);
	}
	catch (EIllegalValue e)
	{
	    return def;
	}
    }
    
    public boolean get_bool(String name) throws EIllegalValue
    {
	String val=get_str(name);
	try
	{
	    return StrUtil.toBool(val);
	}
	catch (NumberFormatException e)
	{
	    throw new EIllegalValue(name,val);
	}
    }
        
    private final String get_str_rec(String key, int depth)
	throws EIllegalValue
    {
	if (depth<1)
	    throw new EInfiniteRecursion(key,get_raw(key));

	/* if we someday want to add special vars/commands, this 
	   would probably be the right point */

	String value;
		boolean notnull = false;
		boolean notempty = false;

		/* handle subclass-defined specials */
	if ((value=handle_special_str(key))!=null)
	    return value;

		/* -- parse special modifiers -- */
		for (boolean parsing=true; parsing;)
		{
			if (key.startsWith("!notnull!"))
			{
				notnull = true;
				key = key.substring(9);
			}
			else if (key.startsWith("!notempty!"))
			{
				notempty = true;
				key = key.substring(10);
			}
			else
				parsing = false;
		}

		if ((value=get_raw(key))==null)
		{
			if (notnull)
				throw new EVariableNull(key,value);
			else
				return "";
		}

	/* --- now replace variables ---  */
	int start, end;
	
	while ((start=value.indexOf("$("))!=-1) /* scan for $( */
	{
	    if ((end=value.indexOf(')', start+2))!=-1)
	    {
		String name = value.substring(start+2,end);
		String newval = 
		    value.substring(0,start)+
		    get_str_rec(name,depth-1)+
		    value.substring(end+1);

		value = newval;
	    }
	    else
		throw new EVariableParseError(key,value);
	}
	
		if (value.equals("") && notempty)
			throw new EVariableEmpty(key,value);

	return value;
    }

    public String dump()
    {
	String res = "";
	for (Enumeration e = db.keys(); e.hasMoreElements(); )
	{
	    String key = (String) e.nextElement();
	    res += key+"=\""+db_get(key)+"\"\n";
        }
	return res;
    }

    public boolean load_content(String field, File filename, boolean strip_comments)
    {
	String text = datasource_cache.loadContent(filename,strip_comments);

	if (text==null)
	    return false;
	set(field,text);
	return true;
    }

    public boolean load_content(String field, URL url, boolean strip_comments)
    {
	String text = datasource_cache.loadContent(url,strip_comments);
	if (text==null)
	    return false;
	
	set(field,text);
	return true;
    }
    
    public Enumeration propertyNames()
    {
	return db.keys();
    }
    
    public IPropertylist clone()
    {
	Propertylist pr = new Propertylist();
	pr.db = (Properties)db.clone();

	// copy the database
	for (Enumeration keys = db.keys(); keys.hasMoreElements(); )
	{
	    String k = (String)keys.nextElement();
	    pr.set(k,db_get(k));
	}
	return pr;
    }
    
    public String toString()
    {
	String str = "[PROPERTYLIST]\n";
	for (Enumeration keys = db.keys(); keys.hasMoreElements(); )
	{
	    String k = (String)keys.nextElement();
	    str += k+"=\""+db_get(k)+"\"\n";
	}
	return str;
    }
}
