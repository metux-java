
package org.de.metux.util;

import java.util.Hashtable;
import java.util.Enumeration;

public class VersionStack
{
    Hashtable index = new Hashtable();

    public VersionStack()
    {
    }

    public VersionStack(String[] v)
    {
	for (int x=0; x<v.length; x++)
	    if (v[x]!=null)
		add(new Version(v[x]));
    }

    public VersionStack(Version[] v)
    {
	for (int x=0; x<v.length; x++)
	    if (v[x]!=null)
		add(v[x]);
    }

    public Version[] getVersions()
    {
	if (index.size()==0)
	    return null;

	Version list[] = new Version[index.size()];    
	int x=0;
	for (Enumeration e=index.keys(); e.hasMoreElements();)
	    list[x++] = (Version)index.get((Version)e.nextElement());
	return list;
    }

    public String toString()
    {
	if (index.size()==0)
	    return null;

	String s = null;
	for (Enumeration e=index.keys(); e.hasMoreElements();)
	{
	    Version v = (Version)index.get((Version)e.nextElement());
	    s = ((s==null)?"":s+" ")+v;
	}
	
	return s;
    }
    
    public boolean containsVersion(Version v)
    {
	Version v2 = (Version)index.get(v);
	if (v2==null)
	    return false;
	    
	if (v2.equals(v))
	    return true;
	    
	throw new RuntimeException("ugh: index mismatch: "+v2+" - "+v);
    }
    
    public boolean add(Version nv)
    {
	index.put(nv,nv);
	return true;
    }
    
    public Version[] getLowers(Version delim)
    {
	VersionStack st = new VersionStack();
	divide(delim,st,null);
	return st.getVersions();
    }
    
    public Version[] getHighers(Version delim)
    {
	VersionStack st = new VersionStack();
	divide(delim,null,st);
	return st.getVersions();
    }

    public int size()
    {
	return index.size();
    }

    public void divide(Version delim, VersionStack lowers, VersionStack highers)
    {
	for (Enumeration e=index.keys(); e.hasMoreElements(); )
	{
	    Version v = (Version)index.get((Version)e.nextElement());
	    switch (v.compareTo(delim))
	    {
		case 0:  break;
		case -1: 
		    if (lowers!=null)  lowers.add(v);  break;
		case 1:  
		    if (highers!=null) highers.add(v); break;
	    }
	}
    }
}

