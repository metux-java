
package org.de.metux.log;

import org.de.metux.util.Terminal;

public class LoggerTerm implements ILogger
{
    public int loglevel = LoglevelID.DEBUG;
    
    final public void setLogLevel(int ll)
    {
	loglevel = ll;
    }
    
    final public void error(String stage, String message)
    {
	if (loglevel >= LoglevelID.ERROR)
	System.err.println(
	    Terminal.render(
		"{TERM_FG_RED} (ERROR) [{TERM_FG_BLUE}"+
		stage+
		"{TERM_FG_RED}] {TERM_FG_YELLOW}"+
		message+
		"{TERM_NORMAL} "
	));
    }
    
    final public void warning(String stage, String message)
    {
	if (loglevel >= LoglevelID.WARNING)
	System.err.println(
	    Terminal.render(
		"{TERM_FG_CYAN}  (WARN) [{TERM_FG_BLUE}"+
		stage+
		"{TERM_FG_CYAN}] "+
		message+
		"{TERM_NORMAL} "
	));
    }
     
    final public void debug(String stage, String message)
    {
	if (loglevel >= LoglevelID.DEBUG)
	System.err.println(
	    Terminal.render(
		"{TERM_FG_YELLOW} (DEBUG) [{TERM_FG_BLUE}"+
		stage+
		"{TERM_FG_YELLOW}] {TERM_NORMAL}"+
		message+
		"{TERM_NORMAL} "
	));
    }
    
    final public void notice(String stage, String message)
    {
	if (loglevel >= LoglevelID.NOTICE)
	System.err.println(
	    Terminal.render(
	        "{TERM_FG_GREEN}(NOTICE) [{TERM_FG_BLUE}"+
		stage+
	        "{TERM_FG_GREEN}] {TERM_FG_YELLOW}"+
	        message+
	        "{TERM_NORMAL} "
	));
    }
}
