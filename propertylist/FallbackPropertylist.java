/* 

    special type of propertylist, which has calls an fallback
    if some key cannot be found. 
    
    this is ie useful to build up derivation hierachies
    
*/

package org.de.metux.propertylist;

import java.util.Properties;

public class FallbackPropertylist extends Propertylist
{
    IPropertylist fallback;

    public FallbackPropertylist(Properties p)
    {
	super();
	loadHash_top(p);    
    }

    public FallbackPropertylist(IPropertylist f)
    {
	fallback = f;
    }

    public String get_raw(String key)
	throws EIllegalValue
    {
	String value = super.get_raw(key);
//	System.err.println("FallbackPropertylist::get_raw(\""+key+"\") => \""+value+"\"");

	if (value!=null) 
	    return value;

	// we have no fallbacks ... jump out
	if (fallback==null)
	    return null;
	    
//	for (int x=0; x<fallback.length; x++)
//	{
//	    if (fallback[x]!=null)
//	    {
///		value = fallback[x].get_raw(key);
//		if (value!=null)
//		    return value;
//	    }
//	}
	return fallback.get_raw(key);
	
//	return null;
    }
    
    public void setFallback(IPropertylist f)
    {
//	fallback = new IPropertylist[1];
//	fallback[0] = f;   
	fallback = f;
    }
    
    public String toString()
    {
	return "[PRIMARY]\n"+super.toString()+
	    ((fallback==null) ? "" : "[FALLBACK]\n"+fallback.toString());
    }
    
    public IPropertylist clone()
    {
//	System.err.println("FallbackPropertylist::clone()");
//	return new FallbackPropertylist(fallback[0]);
	return new FallbackPropertylist(fallback);
    }
}
