
package org.de.metux.propertylist;

public class EProcessingError extends EIllegalValue
{
    public EProcessingError(String my_name, String my_value, Throwable my_cause)
    {
	super(my_name,my_value,my_cause);
    }
}
