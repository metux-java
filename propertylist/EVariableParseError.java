
package org.de.metux.propertylist;

public class EVariableParseError extends EIllegalValue
{
    public EVariableParseError(String name, String value)
    {
	super(name,value);
    }
}
