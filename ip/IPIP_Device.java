
package org.de.metux.ip;

import org.de.metux.util.Exec;
import java.net.UnknownHostException;

public class IPIP_Device 
{
    String if_name;
    
    public IPIP_Device(String name)
    {
	if_name = name;
    }

    public boolean delete()
    {
	return Exec.exec_command("ifconfig "+if_name+" down") && 
	       Exec.exec_command("ip tunnel del "+if_name);
    }

    // fixme: protect interface name !
    public boolean add(
	String local_addr, 
	String remote_addr)
	throws UnknownHostException
    {

	String cmdline = 
	    "ip tunnel add "+if_name+
	    " mode ipip local "+Lookup.getIP_str(local_addr)+
	    " remote "+Lookup.getIP_str(remote_addr)
	;
	
	System.err.println("IPIP::add executing: "+cmdline);
	return Exec.exec_command(cmdline);
    }

    public boolean configureIP(
	String address, 
	String netmask,
	int mtu
    )
	throws UnknownHostException
    {
	return Exec.exec_command(
	    "ifconfig "+if_name+
	    " inet "+Lookup.getIP_str(address)+
	    " netmask "+netmask+
	    " mtu "+mtu);
    }
}
