package org.de.metux.datasource;

import java.util.Hashtable;
import java.util.Properties;
import java.util.Enumeration;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.Reader;
import java.io.IOException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.MalformedURLException;
import org.de.metux.util.Filename;

public class TextTable extends Hashtable<String,Properties>
{
	public String primary_key;

	public TextTable(String pkey)
	{
		primary_key = pkey;
	}

	public TextTable(String pkey, Filename fn)
		throws MalformedURLException, FileNotFoundException, IOException
	{
		primary_key = pkey;
		load(fn);
	}

	private void __store_record(Properties record)
	{
		if (record.size()<1)
			return;

		String k = record.getProperty(primary_key);

		if ((k!=null) && (k.length()>0))
			put(k,record);
		else
			put(record.toString(),record);
	}

	public void loadFrom(BufferedReader in, String name)
		throws IOException
	{
		String line;
		Properties record = new Properties();
		int linecnt = 0;

		while ((line=in.readLine())!=null)
		{
			int x;
			int len;

			/* strip off comments */
			if ((x=line.indexOf('#'))>=0)
				line = line.substring(0,x);

			line = line.trim();
			if ((len=line.length())>0)
			{
				/* -- split into key and value -- */
				if ((x = line.indexOf(':'))>0)
				{
					String key = (line.substring(0,x)).trim();
					String value = (line.substring(x+1)).trim();
					/* if the property already exits, add the new value behind additional newline */
					if (record.containsKey(key))
						record.setProperty(key,record.get(key)+"\n"+value);
					else
						record.setProperty(key,value);
				}
				else if ((line.indexOf("--")==0)&&(!record.isEmpty()))
				{
					__store_record(record);
					record = new Properties();
				}
				else
					System.err.println("CORRUPT LINE ("+name+":"+linecnt+"): "+line);
			}
			linecnt++;
		}
	}

	public void load(Filename fn)
		throws MalformedURLException, FileNotFoundException, IOException
	{
		loadFrom(new BufferedReader(fn.getReader()),fn.toString());
	}

	private String __dump_record(Properties record)
	{
		if (record == null)
			return "";

		String res = "";
		for (Enumeration re = record.keys(); re.hasMoreElements(); )
		{
			String rk = (String)re.nextElement();
			res += rk+": "+record.getProperty(rk)+"\n";
		}
		return res;
	}

	public String dump ()
	{
		String res = "";
		for (Enumeration e = keys(); e.hasMoreElements(); )
		{
			String key = (String)e.nextElement();
			Properties cur = (Properties)get(key);
			res += "#PKEY: "+key+"\n"+__dump_record(cur)+"-- \n";
		}
		return res;
	}
}
