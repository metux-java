#
# this makefile is used for command packages
#

PKGDIR=$(subst .,/,$(PACKAGE))
CLASSPATH=$(BUILDDIR)
CLASS_SOURCES=$(shell for i in $(CLASSES); do echo $$i.java; done)
REAL_CLASS_FILES=$(shell for i in $(CLASSES); do echo $(BUILDDIR)/$(PKGDIR)/$$i.class; done)
REAL_OBJ_FILES=$(shell for i in $(CLASSES); do echo $(BUILDDIR)/$(PKGDIR)/$$i.o; done)
XXX_CLASS_FILES=$(shell for i in $(CLASSES); do echo $(PKGDIR)/$$i.class; done)
XXX_OBJ_FILES=$(shell for i in $(CLASSES); do echo $(PKGDIR)/$$i.o; done)
IMPORT_FILES=$(shell for i in $(IMPORTS); do echo $(BUILDDIR)/$$i.jar; done)
JAVAC=javac

#all:	prepare $(REAL_CLASS_FILES) $(COMMAND)
all:	prepare	$(COMMAND)

.SUFFIXES: .java .class

$(BUILDDIR)$(PACKAGE).jar: $(REAL_CLASS_FILES)
	cd $(BUILDDIR)/ && jar cvf $(PACKAGE).jar $(XXX_CLASS_FILES)
	
prepare:
	mkdir -p $(BUILDDIR)/$(PKGDIR)

$(BUILDDIR)/$(PKGDIR)/%.o:	%.java
	gcj -c $<
	mv *.o $(BUILDDIR)/$(PKGDIR)

$(BUILDDIR)/$(PKGDIR)/%.class:	%.java
	CLASSPATH=$(CLASSPATH) gcj -C $<
	mv *.class $(BUILDDIR)/$(PKGDIR)

$(COMMAND):
	CLASSPATH=$(CLASSPATH) $(JAVAC) $(COMMAND).java

clean:
	rm -f *.o *.class run-unit-test $(REAL_CLASS_FILES) $(REAL_OBJ_FILES) $(BUILDDIR)$(PACKAGE).jar
