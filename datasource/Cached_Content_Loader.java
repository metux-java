
package org.de.metux.datasource;

import java.net.URL;
import java.util.Properties;
import java.io.IOException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.Reader;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URLConnection;

public class Cached_Content_Loader implements IContent_Loader
{
    Properties cache = null;
    public boolean enabled = true;

    private String __load_from_reader(Reader in, boolean strip_comments)
    {
	try
	{
	    BufferedReader input = new BufferedReader(in);
	    String text = "";
	    String line;
	    while ((line=input.readLine())!=null)
	    {
		int x;
		// strip off comments
	        if (strip_comments && ((x=line.indexOf('#'))>-1))
		    line = line.substring(0,x);
		text += line.trim() + "\n";
	    }
	    return text.trim();
	}
	catch (IOException e)
	{
	    return null;
	}
    }

    private String __get_cache(String key)
    {
	if ((cache == null) || (!enabled))
	    return null;

	return (String)cache.get(key);
    }

    private String __set_cache(String key, String content)
    {
	if (!enabled)
	    return content;

	if (cache == null)
	    cache = new Properties();

	if (content == null)
	    cache.remove(key);
	else
	    cache.put(key, content);
	return content;
    }

    public String load(String filename, boolean strip_comments)
    {
	String key = (strip_comments ? "STRIP::" : "NOSTRIP::") + filename;
	String content = __get_cache(key);

	if (content != null)
	    return content;

	try
	{
	    return __set_cache(key, __load_from_reader(new FileReader(filename), strip_comments));
	}
	catch (IOException e)
	{
	    return null;
	}
    }

    public String load(File f, boolean strip_comments)
    {
	String key = (strip_comments ? "STRIP::" : "NOSTRIP::") + f.toString();
	String content = __get_cache(key);

	if (content != null)
	    return content;

	try
	{
	    return __set_cache(key, __load_from_reader(new FileReader(f), strip_comments));
	}
	catch (IOException e)
	{
	    return null;
	}
    }

    public String load(URL url, boolean strip_comments)
    {
	String key = (strip_comments ? "STRIP::" : "NOSTRIP::") + url.toString();
	String content = __get_cache(key);

	if (content != null)
	    return content;

	try
	{
	    URLConnection conn = url.openConnection();
	    conn.setDoOutput(true);

	    String s = __load_from_reader(new InputStreamReader(conn.getInputStream()), strip_comments);
	    cache.put(key, s);
	    return s;
	}
	catch (IOException e)
	{
	    return null;
	}
    }

    public void clearCache()
    {
	cache = null;
    }

    public void setCaching(boolean s)
    {
	enabled = s;
	if (!s)
	    cache = null;
    }
}
