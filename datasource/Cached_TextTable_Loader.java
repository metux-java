package org.de.metux.datasource;

import java.util.Hashtable;
import java.util.Properties;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URI;
import org.de.metux.util.Filename;
import org.de.metux.util.LoadFile;

public class Cached_TextTable_Loader
{
	public boolean enabled = true;

	private TextTable_List cache_texttable = new TextTable_List();

	public TextTable loadTextTable(Filename fn, String pkey)
		throws IOException
	{
		if (!enabled)
			return new TextTable(pkey, fn);

		String key = "["+pkey+"]"+fn;

		/* ---- try to get it from cache ---- */
		TextTable tt = cache_texttable.get(key);
		if (tt != null)
			return tt;

		/* -- not in cache, now try to load ---- */
		tt = new TextTable(pkey, fn);
		cache_texttable.put(key, tt);
		return tt;
	}
}
