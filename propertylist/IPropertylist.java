
package org.de.metux.propertylist;

import java.util.Hashtable;
import java.util.Enumeration;
import java.util.Properties;
import java.net.URL;
import java.io.File;

public interface IPropertylist
{
    public String   dump();

    public void     loadProperties_sub(Properties pr);
    public void     loadProperties_top(Properties pr);

    /* call the postprocessor */
    public void     runPostprocessor() throws EIllegalValue;

    /* field access */
    public void     remove ( String key );
    public void     set ( String key, String value );
    public void     add ( String key, String value );

    public String   get_raw(String name)  throws EIllegalValue;
    public String   get_str(String name)  throws EIllegalValue;
    public String[] get_list(String name) throws EIllegalValue;
    public boolean  get_bool(String name) throws EIllegalValue;
    public boolean  get_bool(String name, boolean def);

    public boolean  load_content(String field, File filename, boolean strip_comments);
    public boolean  load_content(String field, URL url, boolean strip_comments);

    public Enumeration propertyNames();
    
    public IPropertylist clone();
}
