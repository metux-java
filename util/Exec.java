
package org.de.metux.util;

import java.util.Properties;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.InputStreamReader;
import java.io.File;

import java.lang.IllegalThreadStateException;

public class Exec
{
    public interface IExecCallback
    {
	public void OutputLine(String text);
	public void ErrorLine(String text);
	public void debug(String text);
	public void error(String text);
    }

    class default_cb implements IExecCallback
    {
	public void OutputLine(String line)
	{
	    System.out.println(">> "+line);
	}
	public void ErrorLine(String line)
	{
	    System.out.println("!> "+line);
	}
	public void debug(String text)
	{
	}
	public void error(String text)
	{
	    System.err.println("EXEC ERROR: "+text);
	}
    }

    class collect_buf implements IExecCallback
    {
	public String buffer = "";
	boolean add_lf = false;
	
	public void OutputLine(String line)
	{
	    buffer += line+(add_lf ? "\n" : "");
	}
	public void ErrorLine(String line)
	{
	    System.out.println("!> "+line);
	}
	public void debug(String text)
	{
	}
	public void error(String text)
	{
	    System.err.println("EXEC ERROR: "+text);
	}	
    }
        
    IExecCallback 	callback;
    boolean       	running;
    BufferedReader 	stdout;
    BufferedReader	stderr;
    Process		process;

    public int exit_code = 0;
    
    class FetchStdoutThread extends Thread
    {
	public void run()
	{
	    try
	    {
	        String line;
		while ((line=stdout.readLine())!=null)
		callback.OutputLine(line);
	    }
	    catch (IOException e)
	    {
		System.err.println("EXEC (read-stdout) IO EXCEPTION: "+e);
	    }
	}
    }

    class FetchStderrThread extends Thread
    {
	public void run()
	{
	    try
	    {
	        String line;
		while ((line=stderr.readLine())!=null)
		callback.OutputLine(line);
	    }
	    catch (IOException e)
	    {
		System.err.println("EXEC (read-stderr) IO EXCEPTION: "+e);
	    }
	}
    }
    
    public Exec()
    {
	callback = new default_cb();	
    }
    
    public Exec(IExecCallback cb)
    {
	callback = cb;
    }

    // run the command and catch output
    // if it failed, return null
    public String run_catch(String cmdline)
    {
	return run_catch(cmdline,false);
    }

    public String run_catch(String cmdline, boolean lf)
    {
	IExecCallback old_cb = callback;
	collect_buf cb = new collect_buf();
	cb.add_lf = lf;
	callback = cb;
	
	boolean res = run(cmdline);
	callback = old_cb;

	if (res)
	    return cb.buffer;
	else
	{
	    System.err.println("run_catch() error: \""+cb.buffer+"\"");
	    return null;
	}
    }

    private boolean is_interrupted(IOException e)
    {
	String except = e.toString();
	return (except.indexOf("Interrupted system call")!=-1);
    }
    
    public boolean run(String cmdline)
    {
	return run(cmdline,(File)null);
    }

    public boolean run(String cmdline, File dir)
    {
	String cmds[] = cmds = new String[3];

	cmds[0] = "/bin/bash";
	cmds[1] = "-c";
	cmds[2] = cmdline + " 2>&1";
		
	callback.debug("executing: \""+cmdline+"\"");
	
	if (dir==null)
	    dir = new File(System.getProperty("user.dir"));

	try 
	{
	    if (dir==null)
		process = Runtime.getRuntime().exec(cmds);
	    else
		process = Runtime.getRuntime().exec(cmds,null,dir);
	}
	catch (IOException e)
	{
	    callback.error("Stage::exec() could not execute: "+cmdline+
	          " IOException "+e.toString());
		
	    return false;
	}

	stdout = new BufferedReader(new InputStreamReader(process.getInputStream()));
	stderr = new BufferedReader(new InputStreamReader(process.getErrorStream()));

	FetchStderrThread err = new FetchStderrThread();
	FetchStdoutThread out = new FetchStdoutThread();
	
	err.start();
	out.start();
	
	try
	{
	    exit_code = process.waitFor();
	    callback.debug("subprocess exited with: "+exit_code);
	    return (exit_code==0);
	}
	catch (InterruptedException e)
	{
	    callback.error("InterruptedException in Stage::exec()"+e);
	    return false;
	}
    }

    public void abort()
    {
	running = false;
	try { stdout.close(); } 
	catch (IOException e) {	}
	try { stderr.close(); } 
	catch (IOException e) {	}
    }
    
    public static String exec_catch(String cmd)
    {
	return new Exec().run_catch(cmd);
    }
        
    public static boolean exec_command(String cmd)
    {
	return new Exec().run(cmd);
    }

    public static boolean exec_command(String cmd, IExecCallback cb)
    {
	return new Exec(cb).run(cmd);
    }
}
