
package org.de.metux.util;

public class HTMLEntities
{
    // FIXME !
    public static String encode(String str)
    {
	return 
	    StrReplace.replace(" ", "&nbsp;" , 
	    StrReplace.replace("\"", "&quot;", str));
    }
        
    public static String decode(String str)
    {
	return
	    StrReplace.replace("&nbsp;", " ", 
	    StrReplace.replace("&quot;", "\"", str));
    }
}
