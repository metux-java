
package org.de.metux.util;

import java.util.Hashtable;
import java.util.Enumeration;

public class UniqueValues
{
    public static String[] unique(String par[])
    {
	Hashtable ht = new Hashtable();

	if (par==null)
	    return null;

	for (int x=0; x<par.length; x++)
	    ht.put(par[x],par[x]);

	String out[] = new String[ht.size()];
	int a = 0;
	for (Enumeration e=ht.keys(); e.hasMoreElements(); )
	    out[a++] = (String)e.nextElement();
    
	return out;
    }
}
