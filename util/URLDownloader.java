
package org.de.metux.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Properties;
import java.io.InputStreamReader;
import java.io.File;

// FIXME: should introduce a callback interface for UIs

public class URLDownloader
{
    static final String path_curl = "curl";
    
    public interface Notify
    {
	public void progress(int percent, int size_got, int size_total);
	public void msg(String msg);
	public void debug(String msg);
    }
    
    Notify notify;
    
    public URLDownloader()
    {
    }
    
    public URLDownloader(Notify n)
    {
	notify = n;
    }
    
    private void _file_err(String local, String msg)
    {
	if (notify!=null)
	    notify.msg("ERROR: "+msg);
    }
    
    public boolean download(String url,String local, boolean verbose)
    {
	String[] argv = 
	{ 
	    path_curl, 
	    url, 
	    "-o", local, 
	    "-L",
	    "--show-error", 
	    "--stderr", "-",
	    "--verbose"
	};
	
	Process p;
	try 
	{
	    p = Runtime.getRuntime().exec(argv);
	    if (notify!=null)
		notify.debug("executed command");
	}
	catch (IOException e)
	{
	    if (notify!=null)
		notify.msg("Error calling curl: "+e);
	    rm.remove_recursive(local);
	    return false;
	}

	if (notify!=null)
	    notify.debug("now reading from subprocess ...\n");
	    
	BufferedReader br = new BufferedReader(
	    new InputStreamReader(p.getInputStream()));	

	try
	{
	    for (String line=""; (line=br.readLine())!=null;)
	    {
		if (notify!=null)
		    notify.debug("GOT: "+line);
		    
		if ((line.indexOf("< 550") > -1)||
		    (line.indexOf("No such file or directory") > -1))
		{
		    rm.remove_recursive(local);
		    return false;
		}
		    
		if (line.indexOf("< HTTP/1.1 40") > -1)
		{
		    rm.remove_recursive(local);
		    _file_err(local,"object not found: "+url);
		    return false;
		}
	    }
	}
	catch (IOException e)
	{
	    System.err.println("Error while reading from socket: "+e);
	}

	if (!((new File(local)).exists()))
	{
	    _file_err(local,"could not download from: "+url);
	    rm.remove_recursive(local);
	    return false;
	}
	
	// FIXME: should check error code!
	return true;	
    }

    public boolean download(String url, String local)
    {
	return download(url,local,false);
    }
}
