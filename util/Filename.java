package org.de.metux.util;

import java.io.File;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.MalformedURLException;

public class Filename
{
	abstract class Instance
	{
		public abstract String toString();
		abstract URL    toURL() throws MalformedURLException;
		abstract Reader getReader() throws FileNotFoundException, IOException;
	}
	class Instance_Name extends Instance
	{
		String name;
		Instance_Name(String n)
		{
			name = n;
		}
		public String toString()
		{
			return name;
		}
		URL toURL() throws MalformedURLException
		{
			return new URL(name);
		}
		Reader getReader() throws FileNotFoundException, IOException
		{
			return new FileReader(new File(name));
		}
	}
	class Instance_File extends Instance
	{
		File file;
		Instance_File(File f)
		{
			file = f;
		}
		public String toString()
		{
			return file.toString();
		}
		URL toURL() throws MalformedURLException
		{
			return file.toURL();
		}
		Reader getReader() throws FileNotFoundException, IOException
		{
			return new FileReader(file);
		}
	}
	class Instance_URL extends Instance
	{
		URL url;
		Instance_URL(URL u)
		{
			url = u;
		}
		public String toString()
		{
			return url.toString();
		}
		URL toURL() throws MalformedURLException
		{
			return url;
		}
		Reader getReader() throws FileNotFoundException, IOException
		{
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			return new InputStreamReader(conn.getInputStream());
		}
	}

	private Instance instance;

	public Filename(String s)
	{
		/* try whether it's a valid URL */
		try
		{
		    instance = new Instance_URL(new URL(s));
		}
		catch (MalformedURLException e)
		{
		    instance = new Instance_Name(s);
		}
	}

	public Filename(File f)
	{
		instance = new Instance_File(f);
	}

	public Filename(URL u)
	{
		instance = new Instance_URL(u);
	}

	public URL toURL()
		throws MalformedURLException
	{
		return instance.toURL();
	}

	public String toString()
	{
		return instance.toString();
	}

	public Reader getReader()
		throws FileNotFoundException, IOException
	{
		return instance.getReader();
	}
}
