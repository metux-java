
package org.de.metux.util;

import java.io.File;
import java.io.IOException;

public class PathNormalizer
{
    String sysroot;
    String skip[];
    
    public String sysroot_marker = "@@SYSROOT@@";

    public static String basename(String filename)
    {
	if ((filename==null)||(filename.length()==0))
	    return "";
	
	int x = filename.lastIndexOf("/");
	if (x==-1)
	    return filename;
	else 
	    return filename.substring(x+1);
    }

    public static String dirname(String filename)
    {
	if ((filename==null)||(filename.length()==0))
	    return "";

	int x=filename.lastIndexOf("/");

	if ((x==-1)||(x==0))
	    return "";
	else
	    return filename.substring(0,x);
    }

    public static String replace_suffix(String filename, String suffix)
    {
	int pos;
	if ((pos=filename.lastIndexOf("."))<1)
	    return filename+"."+suffix;
    
	return filename.substring(0,pos+1)+suffix;
    }

    // split an filename into name (res[0]) and suffix (res[1])
    // the suffix contains the dot !
    public static String [] split_filename ( String filename )
    {
	int pos;
	String [] res = new String[2];
	if ((pos=filename.lastIndexOf("."))>0)
	{
	    res[0] = filename.substring(0,pos);
	    res[1] = filename.substring(pos);
	}
	else
	{
	    res[0] = filename;
	    res[1] = "";
	}
	return res;
    }

    public static String dirname_slash(String filename)
    {
	return normalize_dir(dirname(filename));
    }

    public static String normalize(String path)
    {
	if (path==null)
	    return "";
	    
	path = StrReplace.replace("//", "/", 
	       StrReplace.replace("/./", "/", path)); 

	if (path.endsWith("/"))
	    return path.substring(0,path.length()-1);
	else
	    return path;
    }

    public static String normalize_dir(String path)
    {
	path = normalize(path);
	if ((path.length()==0) || (path.endsWith("/")))
	    return path;
	else
	    return path+"/";
    }
        
    public PathNormalizer()
    {
    }

    public void setSysroot(String sr)
    {
	sysroot = normalize(sr);
    }
    
    public void addSkip(String sk)
    {
	if ((sk==null)||(sk.length()==0))
	    return;
	
	sk = normalize(sk);
	
	if (skip==null)
	{
	    skip = new String[1];
	    skip[0] = sk;
	    return;
	}
	
	String foo[] = new String[skip.length+1];
	for (int x=0; x<skip.length; x++)
	    foo[x] = skip[x];
	foo[skip.length] = sk;
    }

    // normalize and strip off sysroot
    public String strip_sysroot(String path)
    {
	if (path==null)
	    throw new RuntimeException("passed null path");

	if (sysroot==null)
	    throw new RuntimeException("no sysroot !");

	path = normalize(path);
	if (path.startsWith(sysroot))
	    return path.substring(sysroot.length());
	else if (path.startsWith(sysroot_marker))
	    return path.substring(sysroot_marker.length());
	else
	    return path;
    }

    // encode into sysroot'ed notation 
    // we check what has to be sysroot'ed and add @@SYSROOT@@ there
    public String enc_sysroot(String path)
    {
	path = normalize(path);

	// relative pathes are not SYSROOT'ed
	if (!path.startsWith("/"))
	    return path;

	// what begins with sysroot_marker is already SYSROOT'ed
	if (path.startsWith(sysroot_marker))
	    return path;

	if ((sysroot.length()>2) && (path.startsWith(sysroot)))
	    return sysroot_marker+path.substring(sysroot.length());
	
	if (skip!=null)
	{
	    for (int x=0; x<skip.length; x++)
	    {
		if (path.startsWith(skip[x]))
		    return path;
	    }
	}

	return normalize(sysroot_marker+path);
    }

    public String dec_sysroot(String path)
    {
	return dec_sysroot(path,sysroot);
    }
    
    public String dec_sysroot(String path, String sr)
    {
	path = normalize(path);
	if (!path.startsWith(sysroot_marker))
	    return path;
	
        return normalize(sr+path.substring(sysroot_marker.length()));
    }
}
