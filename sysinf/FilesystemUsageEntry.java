
package org.de.metux.sysinf;

public class FilesystemUsageEntry
{
    public String	filesystem;
    public String	mountpoint;
    public String	type;
    public long		blocksize;
    public long		space_total;
    public long		space_free;
    public long		space_used;
    public int		usage_rate;
}
