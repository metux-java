package org.de.metux.datasource;

import java.io.File;
import java.util.Hashtable;
import java.util.Properties;

import org.de.metux.util.TextDB;

public class Cached_TextDB_Loader implements ITextDB_Loader
{
    public boolean enabled = true;

    Hashtable<String,Properties> cache = null;

    public void clearCache()
    {
	cache = null;
    }

    public void setCaching(boolean e)
    {
	enabled = e;
    }

    private Properties __load(String fn)
    {
	Properties pr = new Properties();
	if (TextDB.LoadIntoHashtable(fn,pr))
	    return pr;
	else
	    return null;
    }

    public Properties load(String filename)
    {
	if (filename == null)
	    return null;

	if (!enabled)
	    return __load(filename);

	Properties pr;

	if (cache == null)
	    cache = new Hashtable<String,Properties>();
	else if ((pr = cache.get(filename)) != null)
	    return pr;

	if ((pr = __load(filename)) != null)
	    cache.put(filename,pr);

	return pr;
    }

    public Properties load(File filename)
    {
	return load(filename.toString());
    }
}
