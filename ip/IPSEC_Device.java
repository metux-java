
package org.de.metux.ip;

import org.de.metux.util.Exec;
import java.net.UnknownHostException;

public class IPSEC_Device
{
    String if_name;
    
    public IPSEC_Device(String name)
    {
	if_name = name;
    }

    public boolean Tunnel_delete()
    {
	return Exec.exec_command("ifconfig "+if_name+" down") && 
	       Exec.exec_command("ipsecadm tunnel del "+if_name);
    }

    public boolean Tunnel_add(
	String local_addr, 
	String remote_addr,
	String spi)
	throws UnknownHostException
    {
	return Exec.exec_command("ipsecadm tunnel add "+if_name+
	    " --local="+Lookup.getIP_str(local_addr)+
	    " --remote="+Lookup.getIP_str(remote_addr)+
	    " --spi="+spi
	);
    }

    public boolean ConfigureIP(
	String address, 
	String netmask,
	int mtu
    )
	throws UnknownHostException
    {
	return Exec.exec_command(
	    "ifconfig "+if_name+
	    " inet "+Lookup.getIP_str(address)+
	    " netmask "+netmask+
	    " mtu "+mtu);
    }
}
