
package org.de.metux.util;

public class StrUtil
{
    public static boolean isEmpty(String str[])
    {
	if ((str==null)||(str.length==0))
	    return true;

	for (int x=0; x<str.length; x++)
	    if (!isEmpty(str[x]))
		return false;
	
	return true;
    }	
	
    public static boolean isEmpty(String str)
    {
	if ((str==null)||(str.length()==0))
	    return true;

	str = str.trim();
	if (str.length()==0)
	    return true;
	    
	return false;
    }

    public static String fix_notnull(String str)
    {
	if (str==null)
	    return "";
	else
	    return str;
    }

    public static String fold(String[] str)
    {
	if (str==null)
	    return "";
	
	String res = "";
	for (int x=0; x<str.length; x++)
	    if (str[x]!=null)
		res += ((res.length()==0) ? "" : " ")+str[x];

	return res;
    }

    public static boolean toBool(String str, boolean def)
    {
	try
	{
	    return toBool(str);
	}
	catch (NumberFormatException e)
	{
	    return def;
	}
    }
	
    public static boolean toBool(String str)
	throws NumberFormatException
    {
	str = str.toLowerCase();
	
	if (str.equals("yes") 		||
	    str.equals("on")  		||
	    str.equals("enabled")	||
	    str.equals("1")		||
	    str.equals("true"))
	    return true;

	if (str.equals("no") 		||
	    str.equals("off")           ||
	    str.equals("disabled") 	||
	    str.equals("0")		||
	    str.equals("false"))
	    return false;
	
	throw new NumberFormatException(str);
    }
    
    public String[] trim(String s[])
    {
	if (s==null)
	    return null;
	if (s.length==0)
	    return s;
	    
	int sz = 0;
	for (int x=0; x<s.length; x++)
	{
	    s[x] = s[x].trim();
	    if (!isEmpty(s[x]))
		sz++;
	}
	
	String n[] = new String[sz];
	int y = 0;
	for (int x=0; x<s.length; x++)
	    if (!isEmpty(s[x]))
		n[y++] = s[x];

	return n;
    }
}
